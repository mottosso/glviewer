### GL Viewer

Not much to see here, but a mismash of various techniques and practices in C++ and OpenGL as I learn the ropes.

![](https://user-images.githubusercontent.com/2152766/64870911-175f1580-d63c-11e9-84dc-e86d640a4cbc.gif)

**Goal**

I don't know C++ nor OpenGL but want to experiment and innovate in the area of interactive 3d manipulators, a.k.a. gizmos, and visualisation; specifically in the area of physics simulation. For that, I need a number of things..

<table>
    <tr>
        <th>General</th>
        <th>Rendering</th>
        <th>UX</th>
    </tr>
    <tr>
        <td>

<!-- General  -->

- [ ] Window
    - [ ] Lazy Refresh
- [ ] Scene Graph
    - [x] Create geometry
    - [x] Create material
    - [x] Create light
    - [x] Create camera
    - [x] Selection model
    - [x] Animation buffer
    - [x] Attributes
    - [x] Hierarchy
    - [ ] Connections
    - [x] Side effects
    - [x] Dependencies
    - [x] Dirty Propagation
    - [ ] Culling (extras)
- [ ] Playback
    - [x] Animation format
    - [x] Bake simulation
    - [ ] Animation curves
    - [ ] Interpolation
    - [ ] Speed factor
    - [ ] [Ozz](http://guillaumeblanc.github.io/ozz-animation)
    - [ ] [Motive](https://google.github.io/motive/)
- [ ] IO
    - [ ] [OBJ](https://github.com/syoyo/tinyobjloader)
    - [ ] [Alembic](https://github.com/alembic/alembic)
    - [ ] [USD](https://github.com/PixarAnimationStudios/USD)
- [ ] Sound (extras)
    - [ ] [SoLoud](https://github.com/jarikomppa/soloud)

</td>
<td>

<!-- Rendering -->

- [ ] Geometry
    - [x] Box
    - [x] Sphere
    - [ ] Capsule
    - [ ] Plane
    - [ ] TriangleMesh
    - [ ] HeightField
- [x] Shading
    - [x] Blinn
- [ ] Lighting
    - [x] Point Light
    - [x] Directional Light
    - [ ] Area Light
    - [ ] Multiple lights
    - [x] Shadow Map
    - [ ] Shadow Volumes
    - [ ] SSAO
- [ ] Display modes
    - [ ] Wireframe
    - [ ] Silhouette
    - [ ] Outline
    - [ ] Volume
- [ ] Visualisation
    - [ ] Trajectories
    - [ ] Velocity
    - [ ] Mass
    - [ ] Limits
    - [ ] Constraints
    - [ ] Forces
    - [ ] Tension (push)
    - [ ] Stress (pull)

</td>
<td>

<!-- UX -->

- [ ] Widgets
    - [x] Timeline
    - [x] Play/Stop/Pause
    - [x] Channel Box
    - [ ] Outliner
    - [ ] Curve Editor
- [ ] Camera
    - [x] Tumble
    - [x] Dolly
    - [x] Track
    - [ ] Walk
- [ ] Manipulators
    - [ ] Position
    - [ ] Orientation
    - [ ] Trajectory
    - [ ] Direction
    - [ ] 1D axis
    - [ ] 2D area
    - [ ] Button
    - [ ] Checkbox
    - [ ] Text
- [ ] Picking
    - [x] ID color buffer
    - [ ] Ray casting

</td>
    </tr>
</table>

<br>

#### Technical Debt

- [ ] Thick debug lines currently drawn with Geometry Shader, could be plain quads
- [ ] Nodes stored in separate vectors, should be stored in one
- [ ] Attributes accessed indirectly from within class definition, should be member variables
- [ ] Nodes currently hold a material. Materials should be a vector of nodes
- [ ] Selection is O(n), traversing through at most all nodes to find the right one, could be O(1) with an std::unsorted_map<UID, SharedNode>
- [ ] Icosphere currently produces 1 vertex per index during subdivision. Could be optimised.

<br>

### Build

**Dependencies**

- Qt 5.x
- PhysX 4.x
- Visual Studio 2015
- Qt VS Tools

I chose Qt due to prior experience working with Python and PyQt. Having performed extensive comparisons between major physics libraries in another life - Bullet, ODE, PhysX and MuJoCo - I chose PhysX for having the most intuitive API, well thought out documentation, non-restrictive licence and performance.

**Build**

With Qt 5.13 configured in your Visual Studio session, build should work out of the box.

> Don't forget the `--recursive` option, as the externals are a submodule of the project

```bash
git clone --recursive https://gitlab.com/mottosso/glviewer.git
msbuild .\glviewer.sln
# Build succeeded.
#     0 Warning(s)
#     0 Error(s)
# 
# Time Elapsed 00:00:08.17
```
