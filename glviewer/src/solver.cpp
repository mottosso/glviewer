#include "solver.h"
#include "util.h"

using namespace physx;


PhysicsScene::PhysicsScene() {
    mFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, mAllocator, mErrorCallback);

    mPvd = PxCreatePvd(*mFoundation);
    PxPvdTransport* transport = PxDefaultPvdSocketTransportCreate(PVD_HOST, 5425, 10);
    mPvd->connect(*transport,PxPvdInstrumentationFlag::eALL);

    mPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *mFoundation, PxTolerancesScale(),true,mPvd);

    PxSceneDesc sceneDesc(mPhysics->getTolerancesScale());
    sceneDesc.gravity = PxVec3(0.0f, -9.81f, 0.0f);
    mDispatcher = PxDefaultCpuDispatcherCreate(2);
    sceneDesc.cpuDispatcher = mDispatcher;
    sceneDesc.filterShader  = PxDefaultSimulationFilterShader;
    mScene = mPhysics->createScene(sceneDesc);

    PxPvdSceneClient* pvdClient = mScene->getScenePvdClient();
    if(pvdClient) {
        pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONSTRAINTS, true);
        pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONTACTS, true);
        pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_SCENEQUERIES, true);
    }

    mMaterial = mPhysics->createMaterial(0.5f, 0.5f, 0.6f);

    // As a reminder for the future
    printf("Adding invisible PhysX plane..\n");
    PxRigidStatic* groundPlane = PxCreatePlane(*mPhysics, PxPlane(0,1,0,0), *mMaterial);
    mScene->addActor(*groundPlane);
}


PhysicsScene::~PhysicsScene() {
    PX_RELEASE(mScene);
    PX_RELEASE(mDispatcher);
    PX_RELEASE(mPhysics);

    if(mPvd) {
        PxPvdTransport* transport = mPvd->getTransport();
        mPvd->release();    mPvd = NULL;
        PX_RELEASE(transport);
    }

    PX_RELEASE(mFoundation);

    printf("PhysX cleaned up.\n");
}

void PhysicsScene::build(const SharedScene& scene) {
    {
        Timer t("Cleared existing scene in %f ms\n");
        if (mScene) mScene->release();

        // TODO: Expose as attributes
        PxSceneDesc sceneDesc(mPhysics->getTolerancesScale());
        sceneDesc.gravity = PxVec3(0.0f, -9.81f, 0.0f);
        sceneDesc.cpuDispatcher = mDispatcher;
        sceneDesc.filterShader  = PxDefaultSimulationFilterShader;

        mScene = mPhysics->createScene(sceneDesc);

        PxPvdSceneClient* pvdClient = mScene->getScenePvdClient();
        if(pvdClient) {
            pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONSTRAINTS, true);
            pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONTACTS, true);
            pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_SCENEQUERIES, true);
        }
    }

    {
        Timer t("Built new scene in %f ms\n");

        // As a reminder for the future
        printf("Adding invisible PhysX plane..\n");
        PxRigidStatic* groundPlane = PxCreatePlane(*mPhysics, PxPlane(0,1,0,0), *mMaterial);
        mScene->addActor(*groundPlane);

        printf("Building new scene..\n");
        for (auto& kv : scene->mNodes) {
            SharedNode& node = kv.second;

            if (node->type() != NodeType::Mesh) continue;

            SharedDagNode& dagnode = std::static_pointer_cast<DagNode>(node);

            auto geometry = dagnode->geometry();
            auto transform = PxTransform(PxMat44(dagnode->worldMatrix().data()));

            if (geometry->type() == GeometryType::Box) {
                auto boxShape = std::static_pointer_cast<BoxGeometry>(geometry);

                PxBoxGeometry geometry(boxShape->dimensions[0],
                                       boxShape->dimensions[1],
                                       boxShape->dimensions[2]);
                PxRigidDynamic* rigid = PxCreateDynamic(
                    *mPhysics, transform, geometry, *mMaterial, 10.0f
                );

                rigid->userData = &node;
                mScene->addActor(*rigid);

            } else if (geometry->type() == GeometryType::Sphere) {
                auto geo = std::static_pointer_cast<SphereGeometry>(geometry);

                PxSphereGeometry geometry(geo->radius);
                PxRigidDynamic* rigid = PxCreateDynamic(
                    *mPhysics, transform, geometry, *mMaterial, 10.0f
                );

                rigid->userData = &node;
                mScene->addActor(*rigid);
            }

            else {
                printf("Could not build %s\n", qPrintable(node->name()));
                continue;
            }

            printf("Created rigid for %s\n", qPrintable(node->name()));
        }

    }
}


void PhysicsScene::bake(const SharedScene& scene) {
    unsigned int frameCount = 0;
    unsigned int poseCount = 0;

    printf("Baking simulation..\n");
    PxU32 actorCount = mScene->getNbActors(PxActorTypeFlag::eRIGID_DYNAMIC |
                                           PxActorTypeFlag::eRIGID_STATIC);
    std::vector<PxRigidActor*> actors { actorCount };
    mScene->getActors(PxActorTypeFlag::eRIGID_DYNAMIC | PxActorTypeFlag::eRIGID_STATIC,
                      reinterpret_cast<PxActor**>(&actors[0]),
                      actorCount);

    assert(scene->frames.size() > 0);

    printf("Baking %d actors..\n", actorCount);
    for (int ii=scene->startTime; ii<=scene->endTime; ii++) {
        step(1.0f / (float)scene->frameRate);
 
        for (auto& actor : actors) {
            SharedNode* node = static_cast<SharedNode*>(actor->userData);

            if (!node) {
                // This rigid didn't have a Node equivalent, that's OK
                continue;
            }

            SharedFrame frame = scene->frames[ii];
            PxTransform pose = actor->getGlobalPose();
 
            frame->nodes[*node] = std::make_shared<Pose>(
                QVector3D(pose.p.x, pose.p.y, pose.p.z),
                QQuaternion(pose.q.w, pose.q.x, pose.q.y, pose.q.z)
            );

            poseCount++;
        }

        frameCount++;
    }

    printf("Baked %d poses in %d frames\n", poseCount, frameCount);
}


void PhysicsScene::step(PxReal timeDelta) {
   mScene->simulate(timeDelta * 0.25f);
   mScene->fetchResults(true);
   mScene->simulate(timeDelta * 0.25f);
   mScene->fetchResults(true);
   mScene->simulate(timeDelta * 0.25f);
   mScene->fetchResults(true);
   mScene->simulate(timeDelta * 0.25f);
   mScene->fetchResults(true);
}


PxRigidDynamic* PhysicsScene::createDynamic(const PxTransform& t,
                                            const PxGeometry& geometry) {
    PxRigidDynamic* dynamic = PxCreateDynamic(*mPhysics, t, geometry, *mMaterial, 10.0f);
    dynamic->setAngularDamping(0.5f);
    mScene->addActor(*dynamic);
    return dynamic;
}


// void keyPress(unsigned char key, const PxTransform& camera) {
//     switch(toupper(key)) {
//     case 'B':   createStack(PxTransform(PxVec3(0,0,stackZ-=10.0f)), 10, 2.0f);                      break;
//     case ' ':   createDynamic(camera, PxSphereGeometry(3.0f), camera.rotate(PxVec3(0,0,-1))*200);   break;
//     }
// }
