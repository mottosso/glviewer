#include <iostream>
#include <chrono>

#include <QDirIterator>
#include <QFileInfo>
#include <QApplication>
#include <QIcon>

#include "util.h"

QString Path::join(const QString& first, const QString& second) {
    return QDir(first).filePath(second);
}


QString Path::resource(QString other) {
    return join(QApplication::applicationDirPath(), join("res", other));
}
