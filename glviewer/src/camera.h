#pragma once

#include <QVector3D>
#include <QMatrix4x4>
#include <memory>

#include "scene.h"


class Camera {
public:
    Camera() {}

    void setNode(SharedNode node) { mNode = node; }
    SharedNode getNode() const { return mNode; }

    void begin();
    void tumble(const float dx, const float dy);
    void track(const float dx, const float dy);
    void dolly(const float dx, const float dy);
    QMatrix4x4 matrix();
    void finish();

private:
    SharedNode mNode;

    // Move around an object
    float mTumbleSensitivity { 0.4f };

    // Move along the camera plane 
    float mTrackingSensitivity { 0.01f };

    // Move towards or away from an object 
    float mDollySensitivity { 0.02f };

    float mDistance { 12.0f };
    float mRy { -120.0f };
    float mRx { -20.0f };
    float mStartRx { 0.0f };
    float mStartRy { 0.0f };
    QVector3D mOffset { QVector3D(0.0f, 2.0f, 0.0f) };

    // Transient values
    QVector3D mStartOffset;
    float mStartDistance;
};

using SharedCamera = std::shared_ptr<Camera>;