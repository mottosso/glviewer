#pragma once

#include <memory>
#include <QDockWidget>
#include <QTableView>
#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include <QStyledItemDelegate>
#include <QStyleOptionViewItem>

class QWidget;
class SceneModel;
class MatrixModel;
class QModelIndex;
class QLabel;
class GenericTableView;
class AttributeFilterModel;

#include "scene.h"
Q_DECLARE_METATYPE(AttributeType)
Q_DECLARE_METATYPE(NodeType)


class ChannelBox : public QDockWidget {
    Q_OBJECT

public:
    ChannelBox(const SharedScene scene, QWidget* parent = nullptr);

protected:
    void onSelectionChanged();
    void onTimeChanged();

private:
    SharedScene mScene;

    struct Widgets {
        QWidget* central;
        QLabel* label;
        GenericTableView* transform;
        GenericTableView* matrix;
    } Widgets;

    struct Models {
        SceneModel* transform;
        MatrixModel* matrix;
        AttributeFilterModel* filter;
    } Models;
};


class ChannelDelegate : public QStyledItemDelegate {
public:
    ChannelDelegate(QWidget* parent = nullptr) : QStyledItemDelegate(parent) {}

    QWidget* createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;

    void setEditorData(QWidget *editor,
                       const QModelIndex &index) const override;

    void setModelData(QWidget *editor,
                      QAbstractItemModel *model,
                      const QModelIndex &index) const override;
};



/* Wraps Scene into a Qt-compatible model */
class SceneModel : public QAbstractTableModel {
    Q_OBJECT

public:
    enum {
        Name = Qt::UserRole,
        Value,
        AttributeType,
        NodeType,

        ChannelBox
    };

    SceneModel(QObject* parent = nullptr) : QAbstractTableModel(parent) {}

    void reset(const SharedNode node = nullptr);


private:
    // SharedScene mScene;
    SharedNode mNode;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
};


class AttributeFilterModel : public QSortFilterProxyModel {
    Q_OBJECT

public:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;
};