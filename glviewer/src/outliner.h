#pragma once

#include <memory>
#include <QDockWidget>
#include <QAbstractTableModel>

class QWidget;
class QTableView;
class OutlinerModel;
class QModelIndex;
class QItemSelection;
class Node;
class Scene;
enum class NodeType;

using SharedNode = std::shared_ptr<Node>;
using SharedScene = std::shared_ptr<Scene>;


class Outliner : public QDockWidget {
    Q_OBJECT

public:
    Outliner(const SharedScene scene, QWidget* parent = nullptr);

protected slots:
    void onSelectionChanged(const QModelIndex &index);
    void onSceneSelectionChanged();

private:
    SharedScene mScene;

    struct Widgets {
        QWidget* central;
        QTableView* table;
    } Widgets;

    struct Models {
        OutlinerModel* table;
    } Models;
};


/* Wraps Scene into a Qt-compatible model */
class OutlinerModel : public QAbstractTableModel {
    Q_OBJECT

public:
    OutlinerModel(const SharedScene scene, QObject* parent = nullptr);

    void reset();

    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;

private:
    SharedScene mScene;

    // Internal copy of all types of nodes
    std::vector<SharedNode> mNodes;
    mutable std::map<const NodeType, QIcon> mIconCache;
};