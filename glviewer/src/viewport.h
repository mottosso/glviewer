#pragma once

#include <iostream>
#include <vector>
#include <functional>
#include <unordered_map>

#include <QApplication>
#include <QOpenGLWidget>
#include <QElapsedTimer>

#include <QOpenGLBuffer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLDebugLogger>
#include <QOpenGLDebugMessage>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLFramebufferObject>
#include <QOpenGLTextureBlitter>

#include "vertex.h"
#include "geometry.h"
#include "scene.h"
#include "camera.h"


struct GLNode {
    GLNode(SharedNode node, size_t count, GLenum mode, bool indexed)
        : node(node),
          count(count),
          mode(mode),
          indexed(indexed),
          vbo(QOpenGLBuffer::VertexBuffer),
          ibo(QOpenGLBuffer::IndexBuffer) {}

    void bind() { vao.bind(); }
    void release() { vao.release(); }

    SharedNode node;
    QOpenGLVertexArrayObject vao;
    QOpenGLBuffer vbo;
    QOpenGLBuffer ibo;
    GLenum mode { GL_TRIANGLES };
    size_t count { 0 };
    bool indexed { false };
};

using Task = std::function<void()>;
using TaskQueue = std::vector<Task>;

struct GLShader : public QOpenGLShaderProgram {
    GLShader(QString name) : QOpenGLShaderProgram(), name(name) {}
    QString name;
};


using SharedGLNode = std::shared_ptr<GLNode>;
using SharedGLShader = std::shared_ptr<GLShader>;


struct GLPass {
    GLPass(QString name) : name(name) {}
    QString name;

    void addSetup(Task task) { _setupTasks.push_back(task); }
    void addTeardown(Task task) { _teardownTasks.push_back(task); }

    void bind() {
        for (auto& task : _setupTasks) task();
    }

    void release() {
        for (auto& task : _teardownTasks) task();
    }

private:
    TaskQueue _setupTasks;
    TaskQueue _teardownTasks;
};

using SharedGLPass = std::shared_ptr<GLPass>;

using Nodes = std::vector<SharedGLNode>;
using ShaderMap = std::unordered_map<SharedGLShader, Nodes>;
using PassMap = std::map<SharedGLPass, ShaderMap>;


struct GLScene {
    ~GLScene() {
        qDebug() << "Destroying GLScene..";
        for (auto& pass : passes) {
            for (auto& shader : pass.second) {
                for (auto& node : shader.second) {
                    node->ibo.release();
                    node->vbo.release();
                    node->vao.release();
                }
            }
        }
    }

    SharedGLPass createPass(QString name) {
        auto pass = std::make_shared<GLPass>(name);
        passes[pass] = ShaderMap();
        passToShaders[name] = pass;
        return pass;
    }

    SharedGLPass& passByName(const QString name) { return passToShaders.at(name); }
    ShaderMap& shadersByName(const QString name) { return passes.at(passByName(name)); }

    PassMap passes;
    std::unordered_map<QString, SharedGLPass> passToShaders;
};


/* Rendering class

This class knows about the scene and OpenGL,
and is able to combine the two into a finished frame.

*/
class Viewport : public QOpenGLWidget,
                 protected QOpenGLFunctions {
    Q_OBJECT

signals:
    void glInitialized();

public:
    Viewport(SharedScene scene, QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    ~Viewport();

    void pick(const QPoint coordinate);
    void highlight(const QPoint coordinate);

protected slots:
    void update();
    void messageLogged(const QOpenGLDebugMessage& msg);

    void onNodeAdded(const SharedNode& node);
    void onShaderAdded(const SharedShader shader);

protected:
    void initializeGL() override;
    void paintGL() override;
    void paintMesh(const SharedGeometry& mesh);
    void resizeGL(int w, int h) override;

    void mousePressEvent(QMouseEvent* e) override;
    void mouseMoveEvent(QMouseEvent* e) override;
    void mouseReleaseEvent(QMouseEvent* e) override;


    void printContextInformation();

private:
    SharedScene mScene;
    GLScene mGLScene;
    Camera mCamera;

    // OpenGL representations of scene data
    std::map<SharedShader, SharedGLShader> mShaders;
    std::map<SharedGeometry, SharedGLNode> mGLNodes;

    std::unordered_map<SharedShader, std::vector<SharedNode>> mNodeByShader;

    int mCurrentFrame = 0;
    unsigned int mFrameCount = 0;

    QElapsedTimer timer;
    qint64 currentTime = 0;
    qint64 previousTime = 0;

    QOpenGLDebugLogger* mDebugLogger = Q_NULLPTR;

    // Shader Information
    QMatrix4x4 mProjection;
    QMatrix4x4 mView;

    QOpenGLFramebufferObject* mIdFrameBuffer;
    unsigned int mDepthMap, mDepthMapFBO;

    // Window state
    bool mMouseIsPressed { false} ;

    // Transient state
    bool mIsPicking;
    QPoint mPickingPosition;
    QPoint mMouseStartPos;
    Qt::MouseButton mMouseButtonPressed;

    // TODO: This isn't right; if the scene destroys the node, it should no longer exist
    // This would maintain its own reference to it despite that happening.
    SharedNode mNodeUnderCursor;

    // Shadow mapping
    const unsigned int mShadowWidth { 2048 };
    const unsigned int mShadowHeight { 2048 };
};
