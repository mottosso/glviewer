#pragma once

#include <map>
#include <memory>
#include <QMainWindow>
#include <QString>
#include <QPixmap>

class Scene;
class QPushButton;
class QWidget;
class QSlider;
class QSpinBox;
class QTimer;
class QShowEvent;
class QDockWidget;

using SharedScene = std::shared_ptr<Scene>; // From scene.h
class Viewport;                             // From viewport.h
class ChannelBox;                           // From channelbox.h
class Outliner;                             // From outliner.h


enum class PlayState {
    Stopped, Playing
};

class ApplicationWindow : public QMainWindow {
    Q_OBJECT

public:
    ApplicationWindow(SharedScene scene, QWidget *parent=nullptr, Qt::WindowFlags flags=Qt::WindowFlags());
    ~ApplicationWindow();

protected:
    void mousePressEvent(QMouseEvent* e) override;
    void mouseMoveEvent(QMouseEvent* e) override;
    void mouseReleaseEvent(QMouseEvent* e) override;
    void keyPressEvent(QKeyEvent* e) override;
    void keyReleaseEvent(QKeyEvent* e) override;
    void showEvent(QShowEvent* e) override;
    bool eventFilter(QObject *target, QEvent *e);

private:
    SharedScene mScene;

    struct Timers {
        QTimer* animate;
    } Timers;

    struct Panels {
        QWidget*        body;
        QWidget*        footer;
    } Panels;

    struct Widgets {
        Viewport*       viewport;
        QPushButton*    playButton;
        QPushButton*    stopButton;
        QPushButton*    rewindButton;
        QSlider*        timeline;
        QSpinBox*       time;
    } Widgets;

    struct Docks {
        Outliner*       outliner;
        ChannelBox*     channelBox;
    } Docks;

    PlayState mPlayState;
    bool mMouseIsPressed { false };
    bool mIsDragging { false };
    bool mIsScrubbing { false };
    float mScrubSensitivity { 0.5f };

    // Transient state
    QPoint mMouseStartPos;
    int mStartTime { 0 };  // Time at the time of pressing the mouse

    void loadStyleSheet();

    // Event handlers
    void onStepped();
    void onPlayClicked();
    void onStopClicked();
    void onRewindClicked();
    void onTimelineChanged(int value);
    void onTimeChanged(int time);
    void onSpinBoxEdited();
    void afterGLInitialized();
};
