#include <QWidget>
#include <QListView>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QBrush>
#include <QIcon>
#include <QColor>
#include <QModelIndex>
#include <QAbstractTableModel>

#include "outliner.h"
#include "scene.h"
#include "util.h"
#include "widgets.h"


Outliner::Outliner(const SharedScene scene, QWidget* parent)
    : QDockWidget(parent),
      mScene(scene) {

    setAttribute(Qt::WA_StyledBackground);

    Models.table = new OutlinerModel(scene);
    Widgets.table = new GenericTableView();
    Widgets.table->setModel(Models.table);

    Widgets.central = new QWidget();
    auto layout1 = new QVBoxLayout(Widgets.central);
    layout1->setContentsMargins(0, 0, 0, 0);
    layout1->setSpacing(0);
    layout1->addWidget(Widgets.table);

    auto smodel = Widgets.table->selectionModel();

    connect(scene.get(), &Scene::selectionChanged,
            this, &Outliner::onSceneSelectionChanged);
    connect(Widgets.table, &GenericTableView::clicked,
            this, &Outliner::onSelectionChanged);

    setWidget(Widgets.central);
}


void Outliner::onSelectionChanged(const QModelIndex &index) {
    auto name = index.data(Qt::DisplayRole).toString();
    SharedNode node = mScene->find(name);
    mScene->select(node);
}


void Outliner::onSceneSelectionChanged() {
    Widgets.table->clearSelection();

    for (auto& node : mScene->selection()) {
        for (int ii=0; ii < Models.table->rowCount(); ii++) {
            auto index = Models.table->index(ii, 0);
            if (index.data(Qt::DisplayRole) == node->name()) {
                Widgets.table->selectionModel()->select(index, QItemSelectionModel::Select);
            }
        }
    }
}


OutlinerModel::OutlinerModel(const SharedScene scene, QObject* parent)
    : QAbstractTableModel(parent),
      mScene(scene) {

    this->reset();
}


void OutlinerModel::reset() {
    beginResetModel();

    for (auto& kv : mScene->mNodes) {
        auto& node = kv.second;
        mNodes.push_back(node);
    }

    endResetModel();
}


int OutlinerModel::rowCount(const QModelIndex& parent) const {
    return mNodes.size();
}


int OutlinerModel::columnCount(const QModelIndex& parent) const {
    return 1;
}


QVariant OutlinerModel::data(const QModelIndex& index, int role) const {
    const SharedNode& node = mNodes.at(index.row());

    if (role == Qt::DisplayRole) {
        return QString(node->name());
    }

    if (role == Qt::DecorationRole) {
        try {
            return mIconCache.at(node->type());

        } catch (std::exception const & ex) {
            QIcon icon;

            if (node->type() == NodeType::Mesh) {
                icon = QIcon(Path::resource("icons/12_16_32.png"));
            }
            else if (node->type() == NodeType::Light) {
                icon = QIcon(Path::resource("icons/18_13_32.png"));
            }
            else if (node->type() == NodeType::Camera) {
                icon = QIcon(Path::resource("icons/0_23_32.png"));
            }
            else if (node->type() == NodeType::Debug) {
                icon = QIcon(Path::resource("icons/23_4_32.png"));
            }
            else {
                icon = QIcon(Path::resource("icons/23_4_32.png"));
            }

            mIconCache[node->type()] = icon;
            return icon;
        }
    }

    return QVariant();
}
