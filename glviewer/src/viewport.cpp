#include <algorithm>

#include <QMouseEvent>
#include <QVector2D>
#include <QVector4D>

#include "viewport.h"


Viewport::Viewport(SharedScene scene, QWidget *parent, Qt::WindowFlags f)
    : QOpenGLWidget(parent, f),
      mScene(scene) {

    setMouseTracking(true);

    this->setUpdateBehavior(QOpenGLWidget::NoPartialUpdate);

    QSurfaceFormat format;
    format.setRenderableType(QSurfaceFormat::OpenGL);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setOption(QSurfaceFormat::DebugContext);
    format.setVersion(4, 3);
    format.setSamples(8);
    format.setSwapInterval(1);
    this->setFormat(format);

    auto width = size().width();
    auto height = size().height();

    auto depthPass = mGLScene.createPass("depth");
    auto idPass = mGLScene.createPass("id");
    auto beautyPass = mGLScene.createPass("beauty");
    auto selectionPass = mGLScene.createPass("selection");

    depthPass->addSetup([&]() {
        glViewport(0, 0, mShadowWidth, mShadowHeight);
        glBindFramebuffer(GL_FRAMEBUFFER, mDepthMapFBO);
        glClear(GL_DEPTH_BUFFER_BIT);

        glFrontFace(GL_CCW);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
    });

    depthPass->addTeardown([&]() {
        glDisable(GL_CULL_FACE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    });

    idPass->addSetup([&]() {
        glViewport(0, 0, size().width(), size().height());

        mIdFrameBuffer->bind();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
    });

    idPass->addTeardown([&]() {
        mIdFrameBuffer->release();
    });

    beautyPass->addSetup([&]() {
        glViewport(0, 0, size().width(), size().height());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        glBindTexture(GL_TEXTURE_2D, mDepthMap);
    });

    beautyPass->addTeardown([&]() {
    });

    selectionPass->addSetup([&]() {
        glViewport(0, 0, size().width(), size().height());
        glDisable(GL_DEPTH_TEST);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    });

    selectionPass->addTeardown([&]() {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_DEPTH_TEST);
    });

    selectionPass->addSetup([]() {
    });

    connect(scene.get(), &Scene::nodeAdded, this, &Viewport::onNodeAdded, Qt::DirectConnection);
    connect(scene.get(), &Scene::shaderAdded, this, &Viewport::onShaderAdded, Qt::DirectConnection);

    timer.start();
}

Viewport::~Viewport() {
    makeCurrent();
    mDebugLogger->stopLogging();

    float durationSec = timer.elapsed() / 1000.0f;
    float averageFps = mFrameCount / durationSec;
    qDebug() << "Rendered" << mFrameCount << "frames in" << durationSec << "seconds"
             << "@" << averageFps << "fps";
}


void Viewport::onNodeAdded(const SharedNode& node) {
    qDebug().noquote() << node->name() << "added";

    SharedGeometry geometry;
    SharedMesh mesh;
    SharedShader shader;

    try {
        geometry = node->findAttribute("geometry")->value<SharedGeometry>();
        mesh = node->findAttribute("mesh")->value<SharedMesh>();
        shader = node->findAttribute("shader")->value<SharedShader>();
    }
    catch (const std::exception& e) {
        // Nothing to draw
        qDebug() << node->name() << "skipped";
        return;
    }

    auto f = context()->functions();

    GLenum mode;
    if (geometry->mode() == GeometryMode::Triangles) {
        mode = GL_TRIANGLES;
    } else if (geometry->mode() == GeometryMode::Lines) {
        mode = GL_LINES;
    } else {
        mode = GL_POINTS;
    }

    size_t count;
    bool indexed;

    if (mesh->indices.size() > 0) {
        count = mesh->indices.size();
        indexed = true;
    } else {
        count = mesh->vertices.size();
        indexed = false;
    }

    auto glnode = std::make_shared<GLNode>(node, count, mode, indexed);

    // Create Vertex Array Object
    glnode->vao.create();
    glnode->vao.bind();
    {
        // Create vertex buffer
        glnode->vbo.create();
        glnode->vbo.bind();
        glnode->vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
        glnode->vbo.allocate(mesh->vertices.data(), mesh->vertices.size() * sizeof(Vertex));

        // Create index buffer
        glnode->ibo.create();
        glnode->ibo.bind();
        glnode->ibo.setUsagePattern(QOpenGLBuffer::StaticDraw);
        glnode->ibo.allocate(mesh->indices.data(), mesh->indices.size() * sizeof(unsigned int));

        f->glEnableVertexAttribArray(0);
        f->glEnableVertexAttribArray(1);
        f->glEnableVertexAttribArray(2);
        f->glVertexAttribPointer(0, Vertex::PositionTupleSize, GL_FLOAT, GL_FALSE, Vertex::stride(), Vertex::positionOffset());
        f->glVertexAttribPointer(1, Vertex::UvTupleSize, GL_FLOAT, GL_FALSE, Vertex::stride(), Vertex::uvOffset());
        f->glVertexAttribPointer(2, Vertex::ColorTupleSize, GL_FLOAT, GL_FALSE, Vertex::stride(), Vertex::colorOffset());
    }

    // Release (unbind) all (in reverse order)
    glnode->vao.release();
    glnode->ibo.release();
    glnode->vbo.release();

    auto& glshader = mShaders.at(shader);

    auto& beauty = mGLScene.shadersByName("beauty");
    auto& id = mGLScene.shadersByName("id");
    auto& depth = mGLScene.shadersByName("depth");

    beauty[glshader].emplace_back(glnode);

    if (node->type() == NodeType::Mesh) {
        depth[glshader].emplace_back(glnode);
        id[glshader].emplace_back(glnode);
    }
}


void Viewport::onShaderAdded(const SharedShader shader) {
    qDebug().noquote() << shader->name() << "added";
    auto glShader = std::make_shared<GLShader>(shader->name());

    for (auto& kv : shader->stages) {
        auto stage = kv.first;
        auto fname = kv.second;

        auto glStage = stage == Shader::Vertex ? QOpenGLShader::Vertex :
                       stage == Shader::Fragment ? QOpenGLShader::Fragment :
                       QOpenGLShader::Geometry;
        glShader->addShaderFromSourceFile(glStage, fname);
    }

    glShader->link();
    glShader->release();

    mShaders[shader] = glShader;
}


void Viewport::initializeGL() {
    qDebug() << "Viewport::initializeGL";

    initializeOpenGLFunctions();
    printContextInformation();
    glClearColor(0.1, 0.1, 0.1, 1);

    // TODO: Move this into a QOpenGLFramebufferObject subclass
    {
        qDebug() << "Creating shadow FBO..";

        // Depth map
        glGenFramebuffers(1, &mDepthMapFBO);

        glGenTextures(1, &mDepthMap);
        glBindTexture(GL_TEXTURE_2D, mDepthMap);
        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     GL_DEPTH_COMPONENT, 
                     mShadowWidth,
                     mShadowHeight,
                     0,
                     GL_DEPTH_COMPONENT,
                     GL_FLOAT,
                     NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

        // Attach
        glBindFramebuffer(GL_FRAMEBUFFER, mDepthMapFBO);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mDepthMap, 0);
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);  
        qDebug() << "Done";
    }

    QOpenGLFramebufferObjectFormat format;
    format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
    format.setMipmap(true);
    format.setTextureTarget(GL_TEXTURE_2D);

    mIdFrameBuffer = new QOpenGLFramebufferObject(size(), format);

    // Setup logging
    connect(this, SIGNAL(frameSwapped()), this, SLOT(update()));

    mDebugLogger = new QOpenGLDebugLogger(this);
    if (mDebugLogger->initialize()) {
        connect(mDebugLogger, SIGNAL(messageLogged(QOpenGLDebugMessage)), this, SLOT(messageLogged(QOpenGLDebugMessage)));
        mDebugLogger->startLogging();
    }

    for (auto& shader : mScene->shaders) onShaderAdded(shader);
    for (auto& kv : mScene->mNodes) onNodeAdded(std::static_pointer_cast<DagNode>(kv.second));

    // No point including initialisation
    mFrameCount = 0;

    emit glInitialized();
}


void Viewport::update() {
    currentTime = timer.nsecsElapsed();
    float deltaTime = (currentTime - previousTime);
    float currentTimeSec = currentTime / 1000000000.0f;
    deltaTime /= 1e9f; // Nanoseconds -> seconds
    previousTime = currentTime;

    // Schedule a redraw
    QOpenGLWidget::update();
}


/* Draw foreground objects first, to avoid overdraw */
// void Viewport::sort() {
//     mNodeByShader.clear();

//     for (auto& node : mScene.mNodes)
//     {
//         auto shader& = node->findAttribute("shader")->data<SharedShader>();
//     }
// }


void Viewport::paintGL() {
    mView = mCamera.matrix().inverted();
    mFrameCount += 1;

    // TODO: Get rid of this
    auto camera = mCamera.matrix();
    auto viewPos = QVector3D(camera(0, 3), camera(1, 3), camera(2, 3));
    auto light = mScene->find("mainLight");
    auto frame = mScene->currentFrame();
    auto pose = frame->nodes[light];
    auto lightPos = pose->pos;
    auto quat = pose->quat;
    QVector3D direction, _;
    quat.getAxes(&_, &_, &direction);  // Always returns 3 vectors, we only want one
    float nearPlane = 0.01f, farPlane = 100.0f;
    auto proj = QMatrix4x4();
    auto view = QMatrix4x4();
    view.lookAt(pose->pos,
                QVector3D(0.0f, 0.0f, 0.0f),
                QVector3D(0.0f, 1.0f, 0.0f));
    proj.ortho(-10.0f, 10.0f, -10.0f, 10.0f, nearPlane, farPlane);
    QMatrix4x4 lightSpaceMatrix = proj * view;

    for (auto& ps : mGLScene.passes) {
        auto& glpass = ps.first;
        auto& glshaders = ps.second;

        glpass->bind();

        for (auto& sn : glshaders) {
            auto& glshader = sn.first;
            auto& glnodes = sn.second;

            glshader->bind();

            glshader->setUniformValue("view", mView);
            glshader->setUniformValue("projection", mProjection);
            glshader->setUniformValue("viewPos", viewPos);
            glshader->setUniformValue("shadowMap", 0);

            glshader->setUniformValue("light.type", 0);
            glshader->setUniformValue("light.position", lightPos);
            glshader->setUniformValue("light.direction", direction);
            glshader->setUniformValue("light.ambient", QVector3D(0.01f, 0.01f, 0.01f));
            glshader->setUniformValue("light.diffuse", QVector3D(1.0f, 1.0f, 1.0f));
            glshader->setUniformValue("light.specular", QVector3D(1.0f, 1.0f, 1.0f));

            glshader->setUniformValue("lightSpaceMatrix", lightSpaceMatrix);

            for (auto& glnode : glnodes) {
                glshader->setUniformValue("model", glnode->node->findAttribute("matrix")->data<QMatrix4x4>());
                glshader->setUniformValue("objectColor", glnode->node->findAttribute("objectColor")->data<QColor>());

                glnode->bind();

                if (glnode->indexed) {
                    glDrawElements(glnode->mode, glnode->count, GL_UNSIGNED_INT, nullptr);
                } else {
                    glDrawArrays(glnode->mode, 0, glnode->count);
                }

                glnode->release();
            }

            glshader->release();
        }

        glpass->release();
    }

    QOpenGLWidget::paintGL();
}


/* Pick node under cursor */
void Viewport::pick(const QPoint coordinate) {
    bool append = Qt::ShiftModifier & qApp->queryKeyboardModifiers();
    mScene->select(mNodeUnderCursor, append);
}


/* Highlight node under cursor */
void Viewport::highlight(const QPoint coordinate) {

    // 3 bytes per pixel (RGB), 1x1 bitmap
    QVector<unsigned char> uid(3);

    float x = coordinate.x();
    float y = coordinate.y();

    mIdFrameBuffer->bind();
        glReadPixels( 
            x, size().height() - y,
            1, 1,
            GL_RGB, GL_UNSIGNED_BYTE, &uid[0]
        );
    mIdFrameBuffer->release();

    if (mNodeUnderCursor) {
        // Remove hovered state
        mNodeUnderCursor->state &= ~NodeState::Hovered;
    }

    // Store in case of eventual pick
    mNodeUnderCursor = mScene->nodeFromId(uid);

    if (mNodeUnderCursor) {
        mNodeUnderCursor->state |= NodeState::Hovered;
    }
}


void Viewport::paintMesh(const SharedGeometry& mesh) {
    auto glshape = mGLNodes.at(mesh);

    glshape->vao.bind();

    if (glshape->indexed) {
        glDrawElements(glshape->mode, glshape->count, GL_UNSIGNED_INT, nullptr);
    } else {
        glDrawArrays(glshape->mode, 0, glshape->count);
    }

    glshape->vao.release();
}


void Viewport::resizeGL(int width, int height) {
    mProjection.setToIdentity();
    mProjection.perspective(45.0f, width / float(height), 0.05f, 1000.0f);
}


// ========================================================
//
// Events
//
// ========================================================


void Viewport::mousePressEvent(QMouseEvent* e) {
    mMouseIsPressed = true;
    mMouseStartPos = e->pos();
    mMouseButtonPressed = e->button();

    mCamera.begin();

    QOpenGLWidget::mousePressEvent(e);
}

void Viewport::mouseMoveEvent(QMouseEvent* e) {
    if (mMouseIsPressed && e->modifiers() == Qt::AltModifier) {
        QPoint deltaPos = e->pos() - mMouseStartPos;
        float dx = deltaPos.x();
        float dy = deltaPos.y();

        if (mMouseButtonPressed == Qt::LeftButton) {
            mCamera.tumble(dx, dy);

        } else if (mMouseButtonPressed == Qt::MiddleButton) {
            mCamera.track(dx, dy);

        } else if (mMouseButtonPressed == Qt::RightButton) {
            mCamera.dolly(dx, dy);
        }
    }

    else {
        highlight(e->pos());
    }

    QOpenGLWidget::mouseMoveEvent(e);
}

void Viewport::mouseReleaseEvent(QMouseEvent* e) {
    mMouseIsPressed = false;
    mCamera.finish();

    QOpenGLWidget::mouseReleaseEvent(e);
}


// =================================================================
// 
// Logging
// 
// =================================================================


void Viewport::printContextInformation() {
    QString glType;
    QString glVersion;
    QString glProfile;

    glType = (context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL";
    glVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));

#define CASE(c) case QSurfaceFormat::c: glProfile = #c; break
    switch (format().profile())
    {
        CASE(NoProfile);
        CASE(CoreProfile);
        CASE(CompatibilityProfile);
    }
#undef CASE

    qDebug().noquote() << glType << glVersion << "-" << glProfile;
}

void Viewport::messageLogged(const QOpenGLDebugMessage& msg) {
    QString error;

    // Format based on severity
    switch (msg.severity())
    {
    case QOpenGLDebugMessage::NotificationSeverity:
        error += "--";
        break;
    case QOpenGLDebugMessage::HighSeverity:
        error += "!!";
        break;
    case QOpenGLDebugMessage::MediumSeverity:
        error += "!~";
        break;
    case QOpenGLDebugMessage::LowSeverity:
        error += "~~";
        break;
    }

    error += " (";

    // Format based on source
#define CASE(c) case QOpenGLDebugMessage::c: error += #c; break
    switch (msg.source())
    {
        CASE(APISource);
        CASE(WindowSystemSource);
        CASE(ShaderCompilerSource);
        CASE(ThirdPartySource);
        CASE(ApplicationSource);
        CASE(OtherSource);
        CASE(InvalidSource);
    }
#undef CASE

    error += " : ";

    // Format based on type
#define CASE(c) case QOpenGLDebugMessage::c: error += #c; break
    switch (msg.type())
    {
        CASE(ErrorType);
        CASE(DeprecatedBehaviorType);
        CASE(UndefinedBehaviorType);
        CASE(PortabilityType);
        CASE(PerformanceType);
        CASE(OtherType);
        CASE(MarkerType);
        CASE(GroupPushType);
        CASE(GroupPopType);
    }
#undef CASE

    error += ")";
    qDebug() << qPrintable(error) << " " << qPrintable(msg.message());
}
