#include <algorithm>
#include <QObject>

#include "scene.h"
#include "util.h"

#define _USE_MATH_DEFINES
#include <math.h> // for M_PI

static const char* DefaultFragmentShader = R"(
    #version 330
    in vec4 position;

    void main() {
      gl_Position = position, 1.0;
    }
)";

static const char* DefaultVertexShader = R"(
    #version 330
    out vec4 color;

    void main() {
        color = vec4(1.0, 0.0, 0.0, 1.0);
    }
)";


Pose::Pose(QVector3D pos, QQuaternion quat)
    : pos(pos), quat(quat) {}


Scene::Scene(const int startTime,
             const int endTime,
             const int frameRate,
             QObject* parent)
    : QObject(parent),
      currentTime(startTime),
      startTime(startTime),
      endTime(endTime),
      minRange(startTime),
      maxRange(endTime),
      frameRate(frameRate) {

    // For the UIDs
    srand(123);

    if (endTime <= startTime) {
        qWarning() << "Clamping range" << startTime << endTime;
        this->endTime = startTime + 1;
    }

    // Allocate memory
    qDebug() << "Allocating memory for frames..";
    for (int ii=startTime; ii<=endTime; ii++) {
        auto frame = std::make_shared<Frame>(ii);
        frames.push_back(frame);
    }

    qDebug() << "Initializing default shaders..";
    createShader("defaultLight", Path::resource("light.vert"), Path::resource("light.frag"));
    createShader("defaultShader", Path::resource("lambert.vert"), Path::resource("lambert.frag"));
    createShader("defaultDebug", Path::resource("debug.vert"), Path::resource("debug.frag"));
    createShader("defaultDepth", Path::resource("depth.vert"), Path::resource("depth.frag"));
    createShader("defaultID", Path::resource("id.vert"), Path::resource("id.frag"));
    createShader("defaultIDLine", Path::resource("idline.vert"),
                                  Path::resource("idline.frag"),
                                  Path::resource("idline.geom"));

    createCamera("defaultCamera");

    qDebug() << "Scene OK";
}


/* Fill frame buffer */
void Scene::reset() {
    qDebug() << "Animating..";

    auto startTime = this->startTime;
    auto endTime = this->endTime;

    for (int ii=startTime; ii<=endTime; ii++) {
        auto frame = frames[ii];

        // TEMP: Store light animation
        float frac = static_cast<float>(ii) / endTime;
        float x = sin(frac * M_PI * 3) * 2.0f;
        float y = 3.0f;
        float z = cos(frac * M_PI * 3) * 2.0f;

        frame->nodes[find("mainLight")] = std::make_shared<Pose>(
            QVector3D(x, y, z),
            QQuaternion::fromDirection(QVector3D(x, y, z), QVector3D(0, 1.0f, 0))
        );
    }

    setCurrentTime(startTime);
    qDebug() << "Finished..";
}


void Scene::setCurrentTime(const int time) {
    int clampedTime = time;

    if (time > endTime) {
        clampedTime = startTime;
    }

    if (time < startTime) {
        clampedTime = endTime;
    }

    SharedFrame frame = frames[clampedTime];

    for (auto& kv : frame->nodes) {
        SharedNode node = kv.first;
        SharedPose pose = kv.second;

        SharedAttribute tx = node->findAttribute("translateX");
        SharedAttribute ty = node->findAttribute("translateY");
        SharedAttribute tz = node->findAttribute("translateZ");
        SharedAttribute qx = node->findAttribute("quaternionX");
        SharedAttribute qy = node->findAttribute("quaternionY");
        SharedAttribute qz = node->findAttribute("quaternionZ");
        SharedAttribute qw = node->findAttribute("quaternionW");
        tx->setValue<float>(pose->pos.x());
        ty->setValue<float>(pose->pos.y());
        tz->setValue<float>(pose->pos.z());
        qx->setValue<float>(pose->quat.x());
        qy->setValue<float>(pose->quat.y());
        qz->setValue<float>(pose->quat.z());
        qw->setValue<float>(pose->quat.scalar());

        // Todo
        // SharedAttribute t = node->findAttribute("translate");
        // SharedAttribute q = node->findAttribute("quaternion");

        // t->setValue<QVector3D>(pose->pos);
        // q->setValue<QQuaternion>(pose->quat);
    }

    for (auto& kv : mNodes) {
        for (auto& av : kv.second->attributes()) {
            av.second->setDirty(true);
        }
    }

    this->currentTime = clampedTime;
    emit timeChanged(clampedTime);
}


SharedNode Scene::nodeFromId(const QVector<unsigned char> id) const {
    SharedNode selected;

    // TODO: Implement this with an unsorted_map<UID, SharedNode>
    for (auto& kv : mNodes) {
        auto& node = kv.second;
        if (node->uid() == id) {
            selected = node;
            break;
        }
    }

    return selected;
}


void Scene::select(const QVector<unsigned char> id, bool append) {
    select(nodeFromId(id), append);
}


void Scene::select(const SharedNode node, bool append) {
    // manipulators.clear();

    if (!append) {
        mSelectionModel.clear();
    }

    if (!node) {
        qDebug() << "Nothing found";
        emit selectionChanged();
        return;
    }

    bool alreadySelected = std::find(mSelectionModel.begin(),
                                     mSelectionModel.end(),
                                     node) != mSelectionModel.end();

    if (!alreadySelected) {
        mSelectionModel.push_back(node);
        qDebug().noquote() << node->name();
    }

    if (node->type() != NodeType::Manipulator) {
        manipulate(node);   
    }

    node->state |= NodeState::Selected;
    emit selectionChanged();
}


void Scene::manipulate(const SharedNode node) {

    // Attribute ----> Manipulator
    qDebug() << "Creating manipulator and callback..";
    // auto manip = createManipulator();
    // auto manip1 = static_cast<Manipulator*>(manip.get());

    // manip->findAttribute("translateX")->setData<float>(node->findAttribute("translateX")->data<float>());
    // manip->findAttribute("translateY")->setData<float>(node->findAttribute("translateY")->data<float>());
    // manip->findAttribute("translateZ")->setData<float>(node->findAttribute("translateY")->data<float>());

    // connect(node.get(), &Node::attributeChanged, [=](Attribute* attr) {
    //     qDebug() << node->name() << "." << attr->name() << "has changed";
    // });

    // // Manipulator ---> Attribute
    // connect(manip, &Manipulator::valueChanged, [=](const float value) {
    //     qDebug() << "I've changed to" << value;
    // });
}


SharedGeometry sharedGeometry(const Geometry& geometry) {
    switch(geometry.type()) {
        case GeometryType::Box:
            return std::make_shared<BoxGeometry>(
                static_cast<const BoxGeometry&>(geometry));

        case GeometryType::Sphere:
            return std::make_shared<SphereGeometry>(
                static_cast<const SphereGeometry&>(geometry));

        case GeometryType::Grid:
            return std::make_shared<GridGeometry>(
                static_cast<const GridGeometry&>(geometry));

        case GeometryType::Origin:
            return std::make_shared<OriginGeometry>(
                static_cast<const OriginGeometry&>(geometry));

        case GeometryType::DirectionalLight:
            return std::make_shared<DirectionalLightGeometry>(
                static_cast<const DirectionalLightGeometry&>(geometry));

        case GeometryType::PointLight:
            return std::make_shared<PointLightGeometry>(
                static_cast<const PointLightGeometry&>(geometry));

        case GeometryType::Camera:
            return std::make_shared<CameraGeometry>(
                static_cast<const CameraGeometry&>(geometry));

        case GeometryType::Manipulator1D:
            return std::make_shared<Manipulator1DGeometry>(
                static_cast<const Manipulator1DGeometry&>(geometry));

        case GeometryType::Null:
            return std::make_shared<NullGeometry>(
                static_cast<const NullGeometry&>(geometry));

        default:
            return std::make_shared<BoxGeometry>();
    }
}


SharedDagNode Scene::createNode(const QString name,
                                const Geometry& geometry,
                                SharedDagNode parent) {
    auto uid = createUid();
    auto node = std::make_shared<DagNode>(name, NodeType::Mesh, uid, parent);

    SharedGeometry sharedGeo = sharedGeometry(geometry);
    auto shader = shaders[1];  // Default lambert

    node->attachGeometry(sharedGeo);
    node->attachShader(shader);

    node->findAttribute("geometry")->setValue<SharedGeometry>(sharedGeo);
    node->findAttribute("mesh")->setValue<SharedMesh>(std::move(sharedGeo->create()));
    node->findAttribute("shader")->setValue<SharedShader>(shader);

    mNodes[name] = node;

    emit nodeAdded(node);

    return node;
}


SharedDagNode Scene::createDebugNode(const QString name,
                                  const Geometry& geometry,
                                  SharedDagNode parent) {
    auto uid = createUid();
    auto node = std::make_shared<DagNode>(name, NodeType::Debug, uid, parent);

    SharedGeometry sharedGeo = sharedGeometry(geometry);
    auto shader = shaders[2];  // Debug shader

    node->attachGeometry(sharedGeo);
    node->attachShader(shader);

    node->findAttribute("geometry")->setValue<SharedGeometry>(sharedGeo);
    node->findAttribute("mesh")->setValue<SharedMesh>(std::move(sharedGeo->create()));
    node->findAttribute("shader")->setValue<SharedShader>(shader);

    mNodes[name] = node;

    emit nodeAdded(node);

    return node;
}


SharedDagNode Scene::createCamera(const QString name) {
    auto uid = createUid();
    auto node = std::make_shared<DagNode>(name, NodeType::Camera, uid);
    
    auto geometry = CameraGeometry();
    SharedGeometry sharedGeo = sharedGeometry(geometry);
    auto shader = shaders[2];  // Debug shader

    node->attachGeometry(sharedGeo);
    node->attachShader(shader);

    node->findAttribute("geometry")->setValue<SharedGeometry>(sharedGeo);
    node->findAttribute("mesh")->setValue<SharedMesh>(std::move(sharedGeo->create()));
    node->findAttribute("shader")->setValue<SharedShader>(shader);

    mNodes[name] = node;

    emit nodeAdded(node);

    return node;
}


SharedDagNode Scene::createLight(const QString name,
                              const Geometry& geometry,
                              SharedDagNode parent) {
    auto uid = createUid();
    auto node = std::make_shared<DagNode>(name, NodeType::Light, uid);

    SharedGeometry sharedGeo = sharedGeometry(geometry);
    auto shader = shaders[2];  // Debug shader

    node->attachGeometry(sharedGeo);
    node->attachShader(shader);

    node->findAttribute("geometry")->setValue<SharedGeometry>(sharedGeo);
    node->findAttribute("mesh")->setValue<SharedMesh>(std::move(sharedGeo->create()));
    node->findAttribute("shader")->setValue<SharedShader>(shader);

    mNodes[name] = node;

    emit nodeAdded(node);

    return node;
}


SharedDagNode Scene::createManipulator() {
    auto name = QString("manip1");
    auto node = std::make_shared<ManipulatorNode>(
        name, NodeType::Manipulator, createUid());

    SharedGeometry sharedGeo = std::make_shared<Manipulator1DGeometry>();
    auto shader = shaders[2];  // Debug shader

    node->attachGeometry(sharedGeo);
    node->attachShader(shader);

    node->findAttribute("geometry")->setValue<SharedGeometry>(sharedGeo);
    node->findAttribute("mesh")->setValue<SharedMesh>(std::move(sharedGeo->create()));
    node->findAttribute("shader")->setValue<SharedShader>(shader);

    mNodes[name] = node;

    emit nodeAdded(node);

    return node;
}


SharedShader Scene::createShader(const QString& name,
                                 const QString& vertexShader,
                                 const QString& fragmentShader) {
    auto shader = std::make_shared<Shader>(name);
    shader->addStage(Shader::Vertex, vertexShader);
    shader->addStage(Shader::Fragment, fragmentShader);
    shaders.emplace_back(shader);

    emit shaderAdded(shader);

    return shader;
}


SharedShader Scene::createShader(const QString& name,
                                 const QString& vertexShader,
                                 const QString& fragmentShader,
                                 const QString& geometryShader) {
    auto shader = std::make_shared<Shader>(name);
    shader->addStage(Shader::Vertex, vertexShader);
    shader->addStage(Shader::Fragment, fragmentShader);
    shader->addStage(Shader::Geometry, geometryShader);
    shaders.emplace_back(shader);

    emit shaderAdded(shader);

    return shader;
}


const UID Scene::createUid() {
    UID color(3);

    color[0] = static_cast<unsigned char>(rand() % 255);
    color[1] = static_cast<unsigned char>(rand() % 255);
    color[2] = static_cast<unsigned char>(rand() % 255);

    if (mUids.count(color) != 0) {
        return createUid();
    }

    mUids.insert(color);
    return color;
}


Node::Node(const QString name, NodeType type, UID uid)
    : mName(name), mType(type), mUid(uid) {

    auto uidx = addAttribute("uidX", "uidx", AttributeType::Integer);
    auto uidy = addAttribute("uidY", "uidy", AttributeType::Integer);
    auto uidz = addAttribute("uidZ", "uidz", AttributeType::Integer);

    // These are permanent and immutable
    uidx->setValue<int>(uid[0]);
    uidy->setValue<int>(uid[1]);
    uidz->setValue<int>(uid[2]);

    for (auto& i : { uidx, uidy, uidz }) {
        i->setLocked(true);
    }
}


DagNode::DagNode(const QString name, NodeType type, UID uid, SharedDagNode parent)
    : Node(name, type, uid), mParent(parent) {

    // Default attributes
    auto tx = addAttribute("translateX", "tx", AttributeType::Float);
    auto ty = addAttribute("translateY", "ty", AttributeType::Float);
    auto tz = addAttribute("translateZ", "tz", AttributeType::Float);
    auto rx = addAttribute("rotateX", "rx", AttributeType::Float);
    auto ry = addAttribute("rotateY", "ry", AttributeType::Float);
    auto rz = addAttribute("rotateZ", "rz", AttributeType::Float);
    auto qx = addAttribute("quaternionX", "qx", AttributeType::Float);
    auto qy = addAttribute("quaternionY", "qy", AttributeType::Float);
    auto qz = addAttribute("quaternionZ", "qz", AttributeType::Float);
    auto qw = addAttribute("quaternionW", "qw", AttributeType::Float);
    auto notes = addAttribute("notes", "notes", AttributeType::String);

    // Complex types
    auto matrix = addAttribute("matrix", "mat", AttributeType::Matrix4x4);
    auto worldMatrix = addAttribute("worldMatrix", "wm", AttributeType::Matrix4x4);
    auto geometry = addAttribute("geometry", "geo", AttributeType::Geometry);
    auto mesh = addAttribute("mesh", "m", AttributeType::Mesh);
    auto shade = addAttribute("shader", "shader", AttributeType::Shader);

    // Visual properties
    auto v = addAttribute("visibility", "v", AttributeType::Boolean);
    auto color = addAttribute("objectColor", "col", AttributeType::Color);

    // Physics attributes
    auto mass = addAttribute("mass", "mass", AttributeType::Float);
    auto friction = addAttribute("friction", "fric", AttributeType::Float);

    // Any change to a quaternion impacts the Euler value
    for (auto& q : { qx, qy, qz, qw }) {
        attributeAffects(q, rx);
        attributeAffects(q, ry);
        attributeAffects(q, rz);
    }

    // Any change to movement affects the matrix
    for (auto& q : { tx, ty, tz, qx, qy, qz, qw }) {
        attributeAffects(q, matrix);
    }

    attributeAffects(matrix, worldMatrix);

    for (auto& r : { rx, ry, rz }) {
        r->setHasSideEffects(true);
    }

    v->setValue<bool>(true);

    // These don't need to be visible to the user
    for (auto& a : { qx, qy, qz, qw, notes}) {
        a->setChannelBox(false);
    }
}


ShaderSet::ShaderSet(const QString name, NodeType type, UID uid)
    : Node(name, type, uid) {

    addAttribute("members", "mbr", AttributeType::Vector);
}


SharedAttribute Node::addAttribute(const QString name,
                                   const QString shortname,
                                   AttributeType type) {
    mAttributes[name] = std::make_shared<Attribute>(name, type, this);
    attributeNames.push_back(name);
    return mAttributes[name];
}

void Node::attributeAffects(const SharedAttribute& whenChanges,
                            const SharedAttribute& isAffected) {
    mAffects[whenChanges].push_back(isAffected);
}


SharedAttribute Node::findAttribute(const QString name) {
    return mAttributes[name];
}


bool Node::compute(Attribute* attr) {
    attr->setDirty(false);

    if (attr->name() == "rotateX" || attr->name() == "rotateY" || attr->name() == "rotateZ") {
        auto quat = QQuaternion(
            findAttribute("quaternionW")->value<float>(),
            findAttribute("quaternionX")->value<float>(),
            findAttribute("quaternionY")->value<float>(),
            findAttribute("quaternionZ")->value<float>()
        );

        auto euler = quat.toEulerAngles();
        if (attr->name() == "rotateX") attr->setValue<float>(euler.x());
        if (attr->name() == "rotateY") attr->setValue<float>(euler.y());
        if (attr->name() == "rotateZ") attr->setValue<float>(euler.z());
    }

    if (attr->name() == "matrix") {
        auto tm = Transform();

        auto tx = findAttribute("translateX")->data<float>();
        auto ty = findAttribute("translateY")->data<float>();
        auto tz = findAttribute("translateZ")->data<float>();
        auto qx = findAttribute("quaternionX")->data<float>();
        auto qy = findAttribute("quaternionY")->data<float>();
        auto qz = findAttribute("quaternionZ")->data<float>();
        auto qw = findAttribute("quaternionW")->data<float>();

        tm.translate(QVector3D(tx, ty, tz));
        tm.rotate(QQuaternion(qw, qx, qy, qz));

        findAttribute("matrix")->setValue<QMatrix4x4>(tm);

        // TODO: Take parenthood into consideration
        findAttribute("worldMatrix")->setValue<QMatrix4x4>(tm);
    }

    return true;
}


bool Node::computeSideEffects(Attribute* attr) {
    if (attr->name() == "rotateX" || attr->name() == "rotateY" || attr->name() == "rotateZ") {
        auto quat = QQuaternion::fromEulerAngles(
            findAttribute("rotateX")->value<float>(),
            findAttribute("rotateY")->value<float>(),
            findAttribute("rotateZ")->value<float>()
        );

        findAttribute("quaternionX")->setData<float>(quat.x());
        findAttribute("quaternionY")->setData<float>(quat.y());
        findAttribute("quaternionZ")->setData<float>(quat.z());
        findAttribute("quaternionW")->setData<float>(quat.scalar());
    }

    return true;
}


bool Node::computeAffected(Attribute* attr) {
    if (attr->name() == "quaternionX" ||
        attr->name() == "quaternionY" ||
        attr->name() == "quaternionZ" ||
        attr->name() == "quaternionW") {
            findAttribute("rotateX")->setDirty(true);
            findAttribute("rotateY")->setDirty(true);
            findAttribute("rotateZ")->setDirty(true);
    }

    return true;
}


Transform DagNode::matrix() {
    auto tm = Transform();

    auto tx = findAttribute("translateX")->data<float>();
    auto ty = findAttribute("translateY")->data<float>();
    auto tz = findAttribute("translateZ")->data<float>();
    auto qx = findAttribute("quaternionX")->data<float>();
    auto qy = findAttribute("quaternionY")->data<float>();
    auto qz = findAttribute("quaternionZ")->data<float>();
    auto qw = findAttribute("quaternionW")->data<float>();

    tm.translate(QVector3D(tx, ty, tz));
    tm.rotate(QQuaternion(qw, qx, qy, qz));

    // Todo
    // auto t = findAttribute("translate")->value<QVector3D>();
    // auto q = findAttribute("quaternion")->value<QQuaternion>();

    return tm;
}

Transform DagNode::worldMatrix() {
    Transform tm = this->matrix();

    // auto parent = this->parent();
    // while (parent) {
    //     tm = parent->matrix() * tm;
    //     parent = parent->parent();
    // }

    return tm;
}

Attribute::Attribute(const QString name,
                     AttributeType type,
                     Node* node) : _name(name), _type(type), _node(node) {
    switch (type) {
        case AttributeType::Integer:    setValue<int>(0); break;
        case AttributeType::Float:      setValue<float>(0.0f); break;
        case AttributeType::Boolean:    setValue<bool>(false); break;
        case AttributeType::Double:     setValue<double>(0.0); break;
        case AttributeType::String:     setValue<QString>(""); break;
        case AttributeType::Vector3:    setValue<QVector3D>(QVector3D()); break;
        case AttributeType::Quaternion: setValue<QQuaternion>(QQuaternion()); break;
        case AttributeType::Color:      setValue<QColor>(QColor()); break;
        case AttributeType::Node:       setValue<SharedNode>(nullptr); break;
        case AttributeType::Mesh:       setValue<SharedMesh>(nullptr); break;
        case AttributeType::Shader:     setValue<SharedShader>(nullptr); break;
        case AttributeType::Geometry:   setValue<SharedGeometry>(nullptr); break;
        case AttributeType::Vector:     setValue<std::vector<Attribute>>({}); break;
        case AttributeType::Matrix4x4:  setValue<QMatrix4x4>(QMatrix4x4()); break;
        default: qDebug("Bad node type!"); exit(1);
    }
}
