#pragma once

#include <ctype.h>
#include "PxPhysicsAPI.h"

#include "scene.h"

#define PVD_HOST "127.0.0.1"
#define PX_RELEASE(x)   if(x)   { x->release(); x = NULL; }


class PhysicsScene {
public:
    PhysicsScene();
    ~PhysicsScene();

    physx::PxRigidDynamic* createDynamic(const physx::PxTransform& t, const physx::PxGeometry& geometry);

    void build(const SharedScene& scene);
    void bake(const SharedScene& scene);
    void step(physx::PxReal timeDelta);

private:
    physx::PxDefaultAllocator      mAllocator;
    physx::PxDefaultErrorCallback  mErrorCallback;

    physx::PxFoundation*           mFoundation = nullptr;
    physx::PxPhysics*              mPhysics    = nullptr;

    physx::PxDefaultCpuDispatcher* mDispatcher = nullptr;
    physx::PxScene*                mScene      = nullptr;

    physx::PxMaterial*             mMaterial   = nullptr;

    physx::PxPvd*                  mPvd        = nullptr;
};