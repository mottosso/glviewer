#include <iostream>
#include <memory>

#include <QHeaderView>
#include <QTableView>
#include <QPainter>
#include <QPixmap>
#include <QRect>
#include <QIcon>
#include <QPoint>
#include <QSize>

#include "widgets.h"


GenericTableView::GenericTableView(QWidget* parent)
    : QTableView(parent) {

    horizontalHeader()->hide();
    verticalHeader()->hide();
    setVerticalScrollMode(QTableView::ScrollPerPixel);
    setHorizontalScrollMode(QTableView::ScrollPerPixel);
    verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    setEditTriggers(QAbstractItemView::AllEditTriggers);
}


void GenericTableView::setModel(QAbstractItemModel *model) {
    QTableView::setModel(model);
    horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
}
