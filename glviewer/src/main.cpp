#include <QApplication>
#include <QIcon>
#include "window.h"
#include "solver.h"
#include "util.h"

class Scene;


int main(int argc, char **argv) {
    QApplication app(argc, argv);

    auto scene = std::make_shared<Scene>(0, 200);  // start, end, fps
    auto physicsScene = std::make_shared<PhysicsScene>();

    auto groundNode = scene->createNode("ground", BoxGeometry(2.0f, 0.05f, 2.0f));
    auto boxNode2 = scene->createNode("box2", BoxGeometry(0.25f, 0.25f, 0.25f));
    auto boxNode3 = scene->createNode("box3", BoxGeometry(0.4f, 0.5f, 0.05f));
    auto sphereNode = scene->createNode("sphere1", SphereGeometry(0.25, 4));
    auto dynamicBoxNode = scene->createNode("dynamicBox", BoxGeometry(0.1f, 0.1f, 0.25f));

    auto lightNode = scene->createLight("mainLight", DirectionalLightGeometry());
    // auto lightNode = scene->createLight("mainLight", PointLightGeometry());

    auto originNode = scene->createDebugNode("origin", OriginGeometry());
    auto grid = scene->createDebugNode("grid", GridGeometry(5.0f, 5.0f));

    // auto manip1 = scene->createManipulator();
    // manip1->findAttribute("translateY")->setData<float>(3.0f);

    // Transform things
    // Cmds::setAttr(groundNode, "translateY", 1.0f);

    groundNode->findAttribute("translateY")->setData<float>(1.0f);
    groundNode->findAttribute("rotateZ")->setData<float>(5.0f);
    boxNode2->findAttribute("translateY")->setData<float>(1.25f);
    boxNode2->findAttribute("rotateY")->setData<float>(30.0f);
    boxNode3->findAttribute("translateY")->setData<float>(2.25f);
    boxNode3->findAttribute("rotateX")->setData<float>(10.0f);
    boxNode3->findAttribute("rotateY")->setData<float>(30.0f);
    boxNode3->findAttribute("rotateZ")->setData<float>(50.0f);
    sphereNode->findAttribute("translateX")->setData<float>(0.5f);
    sphereNode->findAttribute("translateY")->setData<float>(2.5f);
    dynamicBoxNode->findAttribute("translateY")->setData<float>(3.0f);
    dynamicBoxNode->findAttribute("rotateX")->setData<float>(5.0f);
    dynamicBoxNode->findAttribute("rotateY")->setData<float>(-12.0f);

    groundNode->findAttribute("objectColor")->setData<QColor>("#333");
    boxNode2->findAttribute("objectColor")->setData<QColor>("#16e");
    boxNode3->findAttribute("objectColor")->setData<QColor>("#999");
    dynamicBoxNode->findAttribute("objectColor")->setData<QColor>("steelblue");
    sphereNode->findAttribute("objectColor")->setData<QColor>("#666");

    sphereNode->findAttribute("mass")->setData<float>(2.0f);
    sphereNode->findAttribute("friction")->setData<float>(0.75f);

    physicsScene->build(scene);
    physicsScene->bake(scene);

    ApplicationWindow window(scene);

    window.resize(1200, 600);
    window.show();
    window.raise();

    return app.exec();
}