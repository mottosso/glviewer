#include <map>

#include <QStatusBar>
#include <QPushButton>
#include <QSpinBox>
#include <QSlider>
#include <QWidget>
#include <QShowEvent>
#include <QFile>
#include <QVBoxLayout>
#include <QString>
#include <QPoint>  // For mMouseStartPos
#include <QKeyEvent>
#include <QMouseEvent>
#include <QTimer>
#include <QProxyStyle>
#include <QDockWidget>

#include "window.h"
#include "viewport.h"
#include "outliner.h"
#include "channelbox.h"
#include "util.h"


/* Make QSlider jump directly to mouse cursor 

From https://stackoverflow.com/a/26281608

*/
class AbsoluteSliderStyle : public QProxyStyle {
public:
    using QProxyStyle::QProxyStyle;

    int styleHint(QStyle::StyleHint hint,
                  const QStyleOption* option = 0,
                  const QWidget* widget = 0,
                  QStyleHintReturn* returnData = 0) const {
        if (hint == QStyle::SH_Slider_AbsoluteSetButtons)
            return (Qt::LeftButton | Qt::MidButton | Qt::RightButton);
        return QProxyStyle::styleHint(hint, option, widget, returnData);
    }
};


ApplicationWindow::ApplicationWindow(SharedScene scene, QWidget* parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags),
      mScene(scene),
      mIsDragging(false) {

    setWindowTitle("minimaya");

    qDebug() << "Instantiating application window..";

    QWidget* centralWidget = new QWidget(this);

    Panels.body = new QWidget();
    Panels.footer = new QWidget();

    Widgets.viewport = new Viewport(scene);
    Widgets.playButton = new QPushButton(QString(L'\uF5B0'));
    Widgets.stopButton = new QPushButton(QString(L'\uE978'));
    Widgets.rewindButton = new QPushButton(QString(L'\uE845'));
    Widgets.timeline = new QSlider(Qt::Horizontal);
    Widgets.time = new QSpinBox();

    Docks.channelBox = new ChannelBox(scene);
    Docks.outliner = new Outliner(scene);

    Timers.animate = new QTimer(this);

    auto layout1 = new QVBoxLayout(Panels.body);
    layout1->setContentsMargins(0, 0, 0, 0);
    layout1->addWidget(Widgets.viewport);
    
    auto layout2 = new QHBoxLayout(Panels.footer);
    layout2->setContentsMargins(0, 0, 0, 0);
    layout2->setSpacing(5);
    layout2->addWidget(Widgets.timeline);
    layout2->addWidget(Widgets.time);
    layout2->addWidget(Widgets.rewindButton);
    layout2->addWidget(Widgets.playButton);
    layout2->addWidget(Widgets.stopButton);

    auto layout3 = new QVBoxLayout(centralWidget);
    layout3->setContentsMargins(0, 0, 0, 0);
    layout3->setSpacing(0);
    layout3->addWidget(Panels.body, 1);
    layout3->addWidget(Panels.footer);
    
    addDockWidget(Qt::RightDockWidgetArea, Docks.channelBox);
    addDockWidget(Qt::LeftDockWidgetArea, Docks.outliner);

    Widgets.timeline->setStyle(new AbsoluteSliderStyle(Widgets.timeline->style()));
    Widgets.timeline->setTickPosition(QSlider::TicksBelow);
    Widgets.timeline->setRange(scene->startTime, scene->endTime);
    Widgets.time->setRange(scene->startTime, scene->endTime);

    this->setCentralWidget(centralWidget);

    this->connect(Widgets.playButton, &QPushButton::clicked,
                  this, &ApplicationWindow::onPlayClicked);
    this->connect(Widgets.stopButton, &QPushButton::clicked,
                  this, &ApplicationWindow::onStopClicked);
    this->connect(Widgets.rewindButton, &QPushButton::clicked,
                  this, &ApplicationWindow::onRewindClicked);
    this->connect(Widgets.timeline, &QSlider::valueChanged,
                  this, &ApplicationWindow::onTimelineChanged);
    this->connect(Widgets.timeline, &QSlider::sliderPressed,
                  [=]() { mIsDragging = true; });
    this->connect(Widgets.timeline, &QSlider::sliderReleased,
                  [=]() { mIsDragging = false; });
    this->connect(Widgets.time, &QSpinBox::editingFinished,
                  this, &ApplicationWindow::onSpinBoxEdited);

    this->connect(Widgets.viewport, &Viewport::glInitialized,
                  this, &ApplicationWindow::afterGLInitialized);

    this->connect(Timers.animate, &QTimer::timeout,
                  this, &ApplicationWindow::onStepped);

    this->connect(scene.get(), &Scene::timeChanged,
                  this, &ApplicationWindow::onTimeChanged);

    this->loadStyleSheet();
    this->statusBar()->showMessage("Awaiting input..");

    qApp->installEventFilter(this);

    Widgets.viewport->setFocus();
}


bool ApplicationWindow::eventFilter(QObject *target, QEvent *e) {
    // Local keyPressEvent doesn't capture keys when child widgets
    // are in focus. This overrides that, and listens at a global
    // application level. Sadly, it doesn't eliminate keyboard repeat,
    // causing events to get picked up several times when held down

    if (e->type() == QEvent::KeyPress) {
        auto keyEvent = static_cast<QKeyEvent*>(e);
        if (Qt::Key_K == keyEvent->key()) {
            mIsScrubbing = true;
        }
    }

    else if (e->type() == QEvent::KeyRelease) {
        auto keyEvent = static_cast<QKeyEvent*>(e);
        if (Qt::Key_K == keyEvent->key()) {
            mIsScrubbing = false;
        }
    }

    return QMainWindow::eventFilter(target, e);
}


void ApplicationWindow::afterGLInitialized() {
    mScene->reset();
}


void ApplicationWindow::onPlayClicked() {
    if (mPlayState == PlayState::Playing) {
        Timers.animate->stop();
        mPlayState = PlayState::Stopped;
        Widgets.playButton->setText(QString(L'\uF5B0'));
        this->statusBar()->showMessage("Paused..", 2000);
        return;
    }

    Timers.animate->start(1000.0f * (1.0f / mScene->frameRate));
    Widgets.playButton->setText(QString(L'\uE769'));
    mPlayState = PlayState::Playing;
    this->statusBar()->showMessage("Playing..", 2000);
}


void ApplicationWindow::onStopClicked() {
    Timers.animate->stop();
    mScene->setCurrentTime(mScene->startTime);

    Widgets.playButton->setText(QString(L'\uF5B0'));
    mPlayState = PlayState::Stopped;
    this->statusBar()->showMessage("Stopped..", 2000);
}


void ApplicationWindow::onRewindClicked() {
    mScene->setCurrentTime(mScene->startTime);
    this->statusBar()->showMessage("Rewound", 2000);
}

void ApplicationWindow::showEvent(QShowEvent* e) {
}


void ApplicationWindow::onStepped() {
    if (mIsDragging) return;

    mScene->setCurrentTime(mScene->currentTime + 1);
}


/* Timeline moved, including programatically or in response to another signal */
void ApplicationWindow::onTimelineChanged(int value) {
    auto time = Widgets.time;

    time->setValue(value);
    mScene->setCurrentTime(value);
}


void ApplicationWindow::onSpinBoxEdited() {
    auto time = Widgets.time;
    auto timeline = Widgets.timeline;
    timeline->setValue(time->value());
}


/* Time has changed in the scene, outside of user interaction */
void ApplicationWindow::onTimeChanged(int time) {
    auto timeline = Widgets.timeline;
    timeline->setValue(time);
}


/* Read stylesheet off of disk and apply it to the window */
void ApplicationWindow::loadStyleSheet() {
    QFile file(Path::resource("style.css"));
    qDebug() << "Loading stylesheet:" << file.fileName();
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    this->setStyleSheet(file.readAll());
    file.close();
}


ApplicationWindow::~ApplicationWindow() {    
}


void ApplicationWindow::mousePressEvent(QMouseEvent* e) {
    mMouseIsPressed = true;
    mMouseStartPos = e->pos();
    mStartTime = mScene->currentTime;
    bool isNavigating = Qt::AltModifier & e->modifiers();
    
    if (!isNavigating && !mIsScrubbing) {

        // Transform coordinate from Window -> Viewport
        auto localPos = Widgets.viewport->mapFrom(this, e->pos());

        Widgets.viewport->pick(localPos);
    }

    QMainWindow::mousePressEvent(e);
}


void ApplicationWindow::mouseMoveEvent(QMouseEvent* e) {
    if (mMouseIsPressed && mIsScrubbing) {
        QPoint deltaPos = e->pos() - mMouseStartPos;
        int dx = static_cast<int>(deltaPos.x() * mScrubSensitivity);
        int scrubTime = std::max(mScene->startTime, std::min(mScene->endTime, mStartTime + dx));
        mScene->setCurrentTime(scrubTime);
        statusBar()->showMessage("Scubbing..", 5);
    }

    QMainWindow::mouseMoveEvent(e);
}


void ApplicationWindow::mouseReleaseEvent(QMouseEvent* e) {
    mMouseIsPressed = false;
    QMainWindow::mouseReleaseEvent(e);
}


void ApplicationWindow::keyPressEvent(QKeyEvent* e) {
    if (Qt::Key_Space == e->key()) {
        (Widgets.playButton)->click();
    }

    if (Qt::AltModifier & e->modifiers()) {
        if (Qt::Key_V == e->key()) {
            (Widgets.playButton)->click();
        }

        if (Qt::Key_Comma == e->key()) {
            mScene->setCurrentTime(mScene->currentTime - 1);
        }

        if (Qt::Key_Period == e->key()) {
            mScene->setCurrentTime(mScene->currentTime + 1);
        }
    }

    if (Qt::Key_Escape == e->key()) {
        QApplication::instance()->exit();
    }

    QMainWindow::keyPressEvent(e);
}


void ApplicationWindow::keyReleaseEvent(QKeyEvent* e) {
    QMainWindow::keyReleaseEvent(e);
}
