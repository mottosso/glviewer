#pragma once

#include <QVector3D>
#include <QQuaternion>
#include <QMatrix4x4>

class Transform : public QMatrix4x4 {
public:
  Transform() : QMatrix4x4() {}

  Transform(const QVector3D translation,
            const QVector3D orientation,
            const QVector3D scaling = QVector3D(1.0f, 1.0f, 1.0f));

  Transform(const QVector3D translation,
            const QQuaternion orientation,
            const QVector3D scaling = QVector3D(1.0f, 1.0f, 1.0f));

  using QMatrix4x4::operator=;

  const QVector3D translation() const;
  const QVector3D scaling() const;
  const QQuaternion rotation() const;

  void setTranslation(const QVector3D& translate);
  void setRotation(const QQuaternion& rotation);
  void setScale(const QQuaternion& scale);
};
