#include <limits>

#include <QWidget>
#include <QTableView>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QBrush>
#include <QColor>
#include <QLabel>
#include <QModelIndex>
#include <QAbstractTableModel>
#include <QLineEdit>
#include <QDoubleSpinBox>

#include "channelbox.h"
#include "widgets.h"
#include "scene.h"


ChannelBox::ChannelBox(const SharedScene scene, QWidget* parent)
    : QDockWidget(parent),
      mScene(scene) {

    setAttribute(Qt::WA_StyledBackground);
    setFeatures(QDockWidget::NoDockWidgetFeatures);

    Models.transform = new SceneModel();
    Models.filter = new AttributeFilterModel();

    Widgets.label = new QLabel("My Node");
    Widgets.transform = new GenericTableView();

    Widgets.central = new QWidget();
    auto layout1 = new QVBoxLayout(Widgets.central);
    layout1->setContentsMargins(0, 0, 0, 0);
    layout1->setSpacing(0);
    layout1->addWidget(Widgets.label);
    layout1->addWidget(Widgets.transform);

    Widgets.label->setObjectName("NodeName");
    Models.filter->setSourceModel(Models.transform);

    Widgets.transform->setItemDelegate(new ChannelDelegate(this));
    Widgets.transform->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    Widgets.transform->verticalHeader()->setDefaultSectionSize(14);
    Widgets.transform->setModel(Models.filter);

    connect(scene.get(), &Scene::selectionChanged, this, &ChannelBox::onSelectionChanged);
    connect(scene.get(), &Scene::timeChanged, this, &ChannelBox::onTimeChanged);

    setWidget(Widgets.central);
}


void ChannelBox::onSelectionChanged() {
    // Reset everything
    Widgets.label->setText("");

    auto selection = mScene->selection();
    if (selection.size() < 1) {
        Models.transform->reset();
        return;
    }

    // Rewrite everything
    SharedNode node = selection[0];
     Models.transform->reset(node);
    Widgets.label->setText(node->name());
}


void ChannelBox::onTimeChanged() {
    // TODO: Update only the items that changed
    onSelectionChanged();
}



// ===============================================================
//
// Delegate
//
// ===============================================================



QWidget* ChannelDelegate::createEditor(QWidget* parent,
                                       const QStyleOptionViewItem& option,
                                       const QModelIndex& index) const {
    if (index.column() != 1) {
        return QStyledItemDelegate::createEditor(parent, option, index);
    }

    auto type = index.data(SceneModel::AttributeType).value<AttributeType>();

    if (AttributeType::Float == type || AttributeType::Double == type) {
        auto editor = new QDoubleSpinBox(parent);
        editor->setDecimals(3);
        editor->setMinimum(std::numeric_limits<double>::lowest());
        editor->setMaximum(std::numeric_limits<double>::infinity());
        editor->setSingleStep(0.1);

        // Defaults to only commit data on Enter or leaving the cell
        // Let's have it update whenever the value changes, interactively
        connect(editor, QOverload<const QString &>::of(&QDoubleSpinBox::valueChanged), [=]() {
            const_cast<ChannelDelegate*>(this)->commitData(editor);
        });

        return editor;
    }

    return new QLineEdit(parent);
}


void ChannelDelegate::setEditorData(QWidget* editor,
                                    const QModelIndex& index) const {
    auto type = index.data(SceneModel::AttributeType).value<AttributeType>();

    if (AttributeType::Float == type || AttributeType::Double == type) {
        auto value = index.data(SceneModel::Value).toDouble();
        static_cast<QDoubleSpinBox*>(editor)->setValue(value);

    } else {
        auto value = index.data(Qt::DisplayRole).toString();
        static_cast<QLineEdit*>(editor)->setText(value);
    }
}


void ChannelDelegate::setModelData(QWidget *editor,
                                   QAbstractItemModel* model,
                                   const QModelIndex& index) const {
    auto type = index.data(SceneModel::AttributeType).value<AttributeType>();

    if (AttributeType::Float == type || AttributeType::Double == type) {
        auto value = static_cast<QDoubleSpinBox*>(editor)->value();
        model->setData(index, value, Qt::EditRole);

    } else {
        auto value = static_cast<QLineEdit*>(editor)->text();
        model->setData(index, value, Qt::EditRole);
    }
}



// ===============================================================
//
// Model
//
// ===============================================================



void SceneModel::reset(const SharedNode node) {
    beginResetModel();
    mNode = node;
    endResetModel();
}


Qt::ItemFlags SceneModel::flags(const QModelIndex& index) const {
    if (index.column() == 1) {
        auto name = mNode->attributeNames[index.row()];
        auto attr = mNode->findAttribute(name);

        if (attr->locked()) {
            return Qt::NoItemFlags;
        }

        return (
            Qt::ItemIsEnabled |
            Qt::ItemIsSelectable |
            Qt::ItemIsEditable
        );
    }

    return QAbstractTableModel::flags(index);
}


int SceneModel::rowCount(const QModelIndex& parent) const {
    return mNode ? mNode->attributeNames.size() : 0;
}


int SceneModel::columnCount(const QModelIndex& parent) const {
    return 2;
}


QVariant SceneModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid()) return QVariant();

    auto column = index.column();
    auto row = index.row();

    if (!mNode) {
        qDebug() << "This is a bug";
        return QVariant();
    }

    if (role == SceneModel::ChannelBox) {
        auto name = mNode->attributeNames[row];
        auto attr = mNode->findAttribute(name);
        return attr->channelBox();
    }

    if (role == SceneModel::NodeType) {
        QVariant value;
        value.setValue(mNode->type());
        return value;
    }

    if (role == SceneModel::AttributeType) {
        auto name = mNode->attributeNames[row];
        auto attr = mNode->findAttribute(name);
        QVariant value;
        value.setValue(attr->type());
        return value;
    }

    if (column == 0 && role == Qt::DisplayRole) {
        return QString(mNode->attributeNames[row]);
    }

    if (column == 1 && role == Qt::DisplayRole) {
        auto name = mNode->attributeNames[row];
        auto attr = mNode->findAttribute(name);
        auto type = attr->type();

        if (AttributeType::Float == type) {
            return QString::number(attr->data<float>(), 'f', 3);
        }

        else if (AttributeType::Double == type) {
            return QString::number(attr->data<double>(), 'f', 3);
        }

        else if (AttributeType::Integer == type) {
            return QString::number(attr->data<int>());
        }

        else if (AttributeType::Boolean == type) {
            return attr->data<bool>() ? "true" : "false";
        }

        else if (AttributeType::String == type) {
            return attr->data<QString>();
        }

        else if (AttributeType::Color == type) {
            auto vec3 = attr->data<QColor>();
            return QString("%1, %2, %3")
                .arg(QString::number(vec3.red(), 'f', 3))
                .arg(QString::number(vec3.green(), 'f', 3))
                .arg(QString::number(vec3.blue(), 'f', 3));
        }

        else if (AttributeType::Vector3 == type) {
            auto vec3 = attr->data<QVector3D>();
            return QString("%1, %2, %3")
                .arg(QString::number(vec3.x(), 'f', 3))
                .arg(QString::number(vec3.y(), 'f', 3))
                .arg(QString::number(vec3.z(), 'f', 3));
        }

        else if (AttributeType::Quaternion == type) {
            auto vec3 = attr->data<QQuaternion>();
            return QString("%1, %2, %3, %4")
                .arg(QString::number(vec3.x(), 'f', 3))
                .arg(QString::number(vec3.y(), 'f', 3))
                .arg(QString::number(vec3.z(), 'f', 3))
                .arg(QString::number(vec3.scalar(), 'f', 3));
        }
    }

    if (column == 1 && role == SceneModel::Value) {
        auto name = mNode->attributeNames[row];
        auto attr = mNode->findAttribute(name);
        auto type = attr->type();

        if (AttributeType::Float == type) {
            return attr->data<float>();
        }

        else if (AttributeType::Double == type) {
            return attr->data<double>();
        }

        else if (AttributeType::Integer == type) {
            return attr->data<int>();
        }

        else if (AttributeType::Boolean == type) {
            return attr->data<bool>();
        }

        else if (AttributeType::String == type) {
            return attr->data<QString>();
        }

        else if (AttributeType::Vector3 == type) {
            return attr->data<QVector3D>();
        }

        else if (AttributeType::Quaternion == type) {
            return attr->data<QQuaternion>();
        }
    }

    if (column == 1 && role == Qt::ForegroundRole) {
        auto name = mNode->attributeNames[row];
        auto attr = mNode->findAttribute(name);
        return attr->locked() ? QColor("#999") : QColor("#ddd");
    }

    if (column == 1 && role == Qt::BackgroundRole) {
        return QColor("#222");
    }

    if (column == 0 && role == Qt::TextAlignmentRole) {
        // For some reason, biwise-or (|) generates a compiler warning
        // I.e. "illegal conversion" but plus (+) works, oddly enough.
        return Qt::AlignVCenter + Qt::AlignRight;
    }

    return QVariant();
}

bool SceneModel::setData(const QModelIndex& index,
                         const QVariant& value,
                         int role) {
    
    if (!index.isValid()) return false;
    if (role != Qt::EditRole) return false;

    auto name = mNode->attributeNames[index.row()];
    auto attr = mNode->findAttribute(name);
    auto type = attr->type();

    if (AttributeType::Float == type) {
        attr->setData<float>(value.toFloat());
    }

    else if (AttributeType::Double == type) {
        attr->setData<float>(value.toDouble());
    }

    else {
        return false;
    }

    return true;
}


bool AttributeFilterModel::filterAcceptsRow(int sourceRow,
                                            const QModelIndex &sourceParent) const {
    auto index = sourceModel()->index(sourceRow, 0, sourceParent);
    return sourceModel()->data(index, SceneModel::ChannelBox).toBool();
}