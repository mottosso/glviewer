#pragma once

#include <chrono>
#include <iostream>

#define _USE_MATH_DEFINES
#include <math.h> // for M_PI

#include <QDir>
#include <QString>


/* Utilities related to path handling */
namespace Path {
    QString join(const QString& first, const QString& second);
    QString resource(QString other);
}

#define radians(angleInDegrees) ((angleInDegrees) * M_PI / 180.0)
#define degrees(angleInRadians) ((angleInRadians) * 180.0 / M_PI)


struct Timer {
    Timer(std::string message) : message(message) {
        start = std::chrono::high_resolution_clock::now();
    }

    ~Timer() {
        end = std::chrono::high_resolution_clock::now();
        auto duration = end - start;
        printf(message.c_str(), duration.count());
    }

private:
    std::string message; 
    std::chrono::time_point<std::chrono::steady_clock> start, end;
};