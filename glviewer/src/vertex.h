#pragma once

#include <QVector2D>
#include <QVector3D>

using Index = unsigned int;

struct Face {
    Face(const unsigned int a,
         const unsigned int b,
         const unsigned int c)
        : a(a), b(b), c(c) {}
    unsigned int a, b, c;
};


class Vertex
{
public:
    // Constructors
    Q_DECL_CONSTEXPR Vertex() {};
    Q_DECL_CONSTEXPR explicit Vertex(const QVector3D& position);
    Q_DECL_CONSTEXPR Vertex(const QVector3D& position, const QVector3D& color);
    Q_DECL_CONSTEXPR Vertex(const float x,
                            const float y,
                            const float z);
    Q_DECL_CONSTEXPR Vertex(const float x,
                            const float y,
                            const float z,
                            const float u,
                            const float v);
    Q_DECL_CONSTEXPR Vertex(const float x,
                            const float y,
                            const float z,
                            const float r,
                            const float g,
                            const float b);
    // Accessors / Mutators
    Q_DECL_CONSTEXPR const QVector3D& position() const;
    Q_DECL_CONSTEXPR const QVector3D& color() const;
    Q_DECL_CONSTEXPR const QVector3D& uv() const;
    void setPosition(const QVector3D& position);
    void setColor(const QVector3D& color);
    void setUv(const QVector2D& uv);

    // OpenGL Helpers
    static const int PositionTupleSize = 3;
    static const int ColorTupleSize = 3;
    static const int UvTupleSize = 2;
    static Q_DECL_CONSTEXPR void* positionOffset();
    static Q_DECL_CONSTEXPR void* colorOffset();
    static Q_DECL_CONSTEXPR void* uvOffset();
    static Q_DECL_CONSTEXPR int stride();

private:
    QVector3D mPosition;
    QVector3D mColor { 1.0f, 1.0f, 1.0f };
    QVector2D mUv;
};

/*******************************************************************************
 * Inline Implementation
 ******************************************************************************/

// Constructors
Q_DECL_CONSTEXPR inline Vertex::Vertex(const QVector3D& position) : mPosition(position) {}
Q_DECL_CONSTEXPR inline Vertex::Vertex(const QVector3D& position, const QVector3D& color) : mPosition(position), mColor(color) {}
Q_DECL_CONSTEXPR inline Vertex::Vertex(const float x, const float y, const float z)
                                       : mPosition(QVector3D(x, y, z)),
                                         mColor(QVector3D(1.0, 1.0, 1.0)) {}

Q_DECL_CONSTEXPR inline Vertex::Vertex(const float x, const float y, const float z,
                                       const float u, const float v)
                                       : mPosition(QVector3D(x, y, z)),
                                         mUv(QVector2D(u, v)),
                                         mColor(QVector3D(1.0, 1.0, 1.0)) {}

Q_DECL_CONSTEXPR inline Vertex::Vertex(const float x, const float y, const float z,
                                       const float r, const float g, const float b)
                                       : mPosition(QVector3D(x, y, z)),
                                         mColor(QVector3D(r, g, b)) {}

// Accessors / Mutators
Q_DECL_CONSTEXPR inline const QVector3D& Vertex::position() const { return mPosition; }
Q_DECL_CONSTEXPR inline const QVector3D& Vertex::color() const { return mColor; }
Q_DECL_CONSTEXPR inline const QVector3D& Vertex::uv() const { return mUv; }

void inline Vertex::setPosition(const QVector3D& position) { mPosition = position; }
void inline Vertex::setColor(const QVector3D& color) { mColor = color; }
void inline Vertex::setUv(const QVector2D& uv ) { mUv = uv; }

// OpenGL Helpers
Q_DECL_CONSTEXPR inline void* Vertex::positionOffset() { return (void*)offsetof(Vertex, mPosition); }
Q_DECL_CONSTEXPR inline void* Vertex::colorOffset() { return (void*)offsetof(Vertex, mColor); }
Q_DECL_CONSTEXPR inline void* Vertex::uvOffset() { return (void*)offsetof(Vertex, mUv); }
Q_DECL_CONSTEXPR inline int Vertex::stride() { return sizeof(Vertex); }
