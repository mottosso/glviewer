#pragma once

#include <set>
#include <map>
#include <unordered_map>

#include <QObject>
#include <QColor>
#include "geometry.h"

// Forward declarations
class Node;
class DagNode;
class Scene;
class Frame;
class Pose;
class Attribute;

// Typedefs
using SharedNode = std::shared_ptr<Node>;
using SharedDagNode = std::shared_ptr<DagNode>;
using SharedScene = std::shared_ptr<Scene>;
using SharedFrame = std::shared_ptr<Frame>;
using SharedPose = std::shared_ptr<Pose>;
using SharedAttribute = std::shared_ptr<Attribute>;
using UID = QVector<unsigned char>;


// Enable hashing of QString
namespace std {
  template<> struct hash<QString> {
    std::size_t operator()(const QString& s) const {
      return qHash(s);
    }
  };
}

/* Data container for each object on each frame */
struct Pose {
    Pose(QVector3D pos, QQuaternion quat);
    QVector3D pos;
    QQuaternion quat;
};


struct Frame {
    Frame(const int number) : number(number) {}

    int number;
    std::map<SharedNode, std::shared_ptr<Pose>> nodes;
};


struct Material {
    QColor color { "#666" };
    float roughness { 0.5f };
};


struct PhysicsMaterial {
    float density { 1.0f };
    float staticFriction { 1.0f };
    float dynamicFriction { 1.0f };
    float restitution { 0.25f };
};


enum class NodeType {
    Mesh, Light, Camera, Debug, Shader, Manipulator
};


enum NodeState {
    None      = 1 << 0,
    Selected  = 1 << 1,
    Active    = 1 << 2,
    Hovered   = 1 << 3
};

// Enable bitwise operators
inline NodeState operator|(NodeState a, NodeState b) {
    return static_cast<NodeState>(static_cast<int>(a) | static_cast<int>(b));
}

inline NodeState operator~(NodeState a) {
    return static_cast<NodeState>(~static_cast<int>(a));
}

inline NodeState& operator|=(NodeState &a, NodeState b) {
    a = static_cast<NodeState>(static_cast<int>(a) | static_cast<int>(b));
    return a;
}

inline NodeState operator&(NodeState a, NodeState b) {
    return static_cast<NodeState>(static_cast<int>(a) & static_cast<int>(b));
}

inline NodeState& operator&=(NodeState &a, NodeState b) {
    a = static_cast<NodeState>(static_cast<int>(a) & static_cast<int>(b));
    return a;
}


enum class AttributeType {
    Float, Double, String, Boolean, Integer,

    // Compound types
    Vector2, Vector3, Vector4, Quaternion, Color, Matrix4x4,

    // Complex types
    Node, Mesh, Shader, Geometry, Vector
};


class Attribute {
public:
    Attribute(QString name, AttributeType type, Node* node);
    ~Attribute() {
        if (_destructor) {
            _destructor(_value);
        }

        delete[] _value;
    }

    const QString name() const { return _name; }
    const AttributeType type() const { return _type; }

    // Whether to display this attribute in the channel box
    const bool channelBox() const { return _channelBox; }
    void setChannelBox(const bool value) { _channelBox = value; }

    // A locked attribute cannot be edited
    const bool locked() const { return _locked; }
    void setLocked(const bool value) { _locked = value; }

    void setDirty(const bool value) { _isDirty = value; }
    void setHasSideEffects(const bool value) { _hasSideEffects = value; }
    SharedNode node() const { return _node; }

    /* High-level modification of data, triggers side effects */
    template<typename T> void setData(const T& value) {
        setValue<T>(value);

        // Trigger side-effects
        _node->computeAffected(this);
        if (_hasSideEffects) _node->computeSideEffects(this);

        emit _node->attributeChanged(this);
    }

    /* Low-level modification of data, does NOT trigger side effects */
    template<typename T> void setValue(const T& value) {
        if (_destructor) _destructor(_value);

        delete[] _value;
        _value = new char[sizeof(T)];
        new(_value) T{value};
        _destructor = &destructor<T>;
    }

    template<typename T> T& value() {
        return *reinterpret_cast<T*>(_value);
    }

    template<typename T> T& data() {
        if (_isDirty) _node->compute(this);
        return value<T>();
    }

    template<typename T> static void destructor(char* data) { reinterpret_cast<T*>(data)->~T(); }

private:
    QString _name;
    char* _value { nullptr };
    bool _keyable { true };
    bool _locked { false };
    bool _channelBox { true };
    bool _isDirty { true };
    bool _isConnected { true };
    bool _hasSideEffects { false };

    AttributeType _type;
    SharedNode _node;

    void (*_destructor)(char*) { nullptr };
};


enum class EventType {
    Pressed,
    Selected,
    Deselected,
    Hovered,
    Moved,
    Released,
    Changed
};


struct Event {
    EventType type;
    std::function<void()> callback;
};


class Node : public QObject {
    Q_OBJECT

signals:
    void attributeChanged(Attribute* attr);

public:
    Node(const QString name, NodeType type, UID uid);

    bool compute(Attribute* attr);
    bool computeSideEffects(Attribute* attr);
    bool computeAffected(Attribute* attr);

    SharedAttribute addAttribute(const QString name,
                                 const QString shortname,
                                 AttributeType type);
    SharedAttribute findAttribute(const QString name);
    bool hasAttribute(const QString name) { return mAttributes.count(name) > 0; }
    void attributeAffects(const SharedAttribute& whenChanges,
                          const SharedAttribute& isAffected);

    // Accessors
    const QString name() const { return mName; }
    const NodeType type() const { return mType; }
    const UID uid() const { return mUid; }

    const std::map<QString, SharedAttribute>& attributes() const { return mAttributes; }

    std::vector<QString> attributeNames;

    NodeState state;

protected:
    QString mName;
    NodeType mType;

    std::map<QString, SharedAttribute> mAttributes;
    std::map<SharedAttribute, std::vector<SharedAttribute>> mAffects;

    UID mUid { 3 };
};


class DagNode : public Node {
public:
    DagNode(const QString name, NodeType type, UID uid, SharedDagNode parent = nullptr);

    Transform matrix();
    Transform worldMatrix();

    void attachGeometry(const SharedGeometry geometry) { mGeometry = geometry; }
    void attachShader(const SharedShader shader) { mShader = shader; }

    const SharedDagNode parent() const { return mParent; }
    const SharedGeometry geometry() const { return mGeometry; }
    const SharedShader shader() const { return mShader; }

protected:
    SharedDagNode mParent;
    SharedGeometry mGeometry;
    SharedShader mShader;
};


class ManipulatorNode : public DagNode {
    Q_OBJECT

signals:
    void valueChanged(const float value);

public:
    using DagNode::DagNode;
};


class ShaderNode : public Node {
    Q_OBJECT

public:
    using Node::Node;
};



class ShaderSet : public Node {
    Q_OBJECT

public:
    ShaderSet(const QString name, NodeType type, UID uid);
};



class Scene : public QObject {
    Q_OBJECT

signals:
    void timeChanged(int time);
    void selectionChanged();

    void nodeAdded(const SharedNode& node);
    void nodeRemoved(const SharedNode& node);

    void shaderAdded(const SharedShader shader);
    void shaderRemoved(const SharedShader shader);

public:
    Scene(const int startTime, const int endTime, const int frameRate = 24, QObject* parent = nullptr);
    ~Scene() {}

    SharedDagNode createNode(const QString name, const Geometry& shape, SharedDagNode parent = nullptr);
    SharedDagNode createDebugNode(const QString name, const Geometry& shape, SharedDagNode parent = nullptr);
    SharedDagNode createLight(const QString name, const Geometry& shape, SharedDagNode parent = nullptr);
    SharedShader createShader(const QString& name,
                              const QString& vertexShader,
                              const QString& fragmentShader);
    SharedShader createShader(const QString& name,
                              const QString& vertexShader,
                              const QString& fragmentShader,
                              const QString& geometryShader);
    SharedDagNode createCamera(const QString name);
    SharedDagNode createManipulator();

    const UID createUid();

    void reset();
    SharedNode find(QString name) { return mNodes.at(name); }

    void setCurrentTime(const int time);
    SharedNode nodeFromId(const QVector<unsigned char> id) const;
    void select(const QVector<unsigned char> id, bool append = false);
    void select(const SharedNode node, bool append = false);
    void manipulate(const SharedNode node);

    std::vector<SharedNode> selection() { return mSelectionModel; }

    // Accessors
    SharedFrame currentFrame() const { return frames[currentTime]; }

    std::vector<SharedShader> shaders;
    std::vector<SharedFrame> frames;

    std::unordered_map<QString, SharedNode> mNodes;

    int frameRate;
    int currentTime;
    int startTime;
    int endTime;
    int minRange;
    int maxRange;

private:
    std::vector<SharedNode> mSelectionModel;
    std::set<UID> mUids;
};
