#pragma once

#include <QTableView>
#include <QPoint>
#include <QString>

// Forward declarations
class QWidget;
class QPixmap;
class QIcon;
class QAbstractItemModel;


class GenericTableView : public QTableView {
    Q_OBJECT

public:
    GenericTableView(QWidget* parent = nullptr);
    void setModel(QAbstractItemModel* model) override;
};
