#include <fstream>
#include <string>
#include <sstream>
#include <QGuiApplication>
#include <QDir>

#define _USE_MATH_DEFINES
#include <math.h> // for M_PI

#include "geometry.h"
#include "util.h"


/*! A plain box

    6 _______ 5
     /      /|
  3 /_____ 2 |
   |  |___|__|
   | / 7  | / 4
   |/_____|/
   0        1

*/
SharedMesh BoxGeometry::create() {
    std::vector<Vertex> vertices = {
               // Position       // UV
        Vertex(-1.0, -1.0, -1.0, 0.0f, 0.0f),  // 0
        Vertex( 1.0, -1.0, -1.0, 1.0f, 0.0f),  // 1
        Vertex( 1.0,  1.0, -1.0, 1.0f, 1.0f),  // 2
        Vertex(-1.0,  1.0, -1.0, 0.0f, 1.0f),  // 3
                            
        Vertex( 1.0, -1.0,  1.0, 1.0f, 0.0f),  // 4
        Vertex( 1.0,  1.0,  1.0, 1.0f, 1.0f),  // 5
        Vertex(-1.0,  1.0,  1.0, 0.0f, 1.0f),  // 6
        Vertex(-1.0, -1.0,  1.0, 0.0f, 1.0f)   // 7
    };

    std::vector<Index> indices = {
        0, 1, 2, // Front
        2, 3, 0,

        5, 4, 7, // Back
        7, 6, 5,

        0, 7, 4, // Bottom
        4, 1, 0,

        6, 3, 2, // Top
        2, 5, 6,

        2, 1, 4, // Right
        4, 5, 2,

        6, 7, 0, // Left
        0, 3, 6,
    };

    for (auto& vertex : vertices) {
        auto newPosition = vertex.position() * this->dimensions;
        vertex.setPosition(newPosition);
    }

    return std::make_shared<Mesh>(vertices, indices);
}


/* An icosphere

         _____
       .\ \/ /\.
      / /\/_/\ \\
     |\/ /\ \ \/|
      \\/\ \_\/_/
       \._\/__\/

*/
QVector3D SphereGeometry::projectPoint(const QVector3D& point) const {
    return point / sqrt(point[0] * point[0] +
                        point[1] * point[1] +
                        point[2] * point[2]);
}


Index SphereGeometry::getMiddlePoint(Index a,
                                     Index b,
                                     std::vector<Vertex>& vertices) {
    QVector3D va = vertices[a].position();
    QVector3D vb = vertices[b].position();
    QVector3D midPoint = (va + vb) / 2;
    vertices.emplace_back(projectPoint(midPoint));
    return static_cast<Index>(vertices.size() - 1);
}


SharedMesh SphereGeometry::create() {
    float t = (1.0f + sqrt(5.0)) / 2.0f;

    std::vector<QVector3D> positions {
        QVector3D(-1.0f,  t,     0.0f),
        QVector3D( 1.0f,  t,     0.0f),
        QVector3D(-1.0f, -t,     0.0f),
        QVector3D( 1.0f, -t,     0.0f),
        QVector3D( 0.0f, -1.0f,  t),
        QVector3D( 0.0f,  1.0f,  t),
        QVector3D( 0.0f, -1.0f, -t),
        QVector3D( 0.0f,  1.0f, -t),
        QVector3D( t,     0.0f, -1.0f),
        QVector3D( t,     0.0f,  1.0f),
        QVector3D(-t,     0.0f, -1.0f),
        QVector3D(-t,     0.0f,  1.0f)      
    };

    std::vector<Vertex> vertices;
    for (auto& pos : positions) {
        vertices.emplace_back(projectPoint(pos));
    }

    std::vector<Index> indices {
        5,  11, 0,
        1,  5,  0,
        7,  1,  0,
        10, 7,  0,
        11, 10, 0,
        9,  5,  1,
        4,  11, 5,
        2,  10, 11,
        6,  7,  10,
        8,  1,  7,
        4,  9,  3,
        2,  4,  3,
        6,  2,  3,
        8,  6,  3,
        9,  8,  3,
        5,  9,  4,
        11, 4,  2,
        10, 2,  6,
        7,  6,  8,
        1,  8,  9
    };

    // Subdivide
    for (int _ = 0; _ < (subdivisions > 1 ? subdivisions : 1) ; _++) {
        std::vector<Index> faces2;
        size_t size = indices.size();

        for (int idx = 0; idx < size / 3; idx++) {
            // replace triangle by 4 triangles
            Index fa = indices[idx * 3 + 0];
            Index fb = indices[idx * 3 + 1];
            Index fc = indices[idx * 3 + 2];
            Index a = getMiddlePoint(fa, fb, vertices);
            Index b = getMiddlePoint(fb, fc, vertices);
            Index c = getMiddlePoint(fc, fa, vertices);

            faces2.push_back(fa);
            faces2.push_back(a);
            faces2.push_back(c);
            faces2.push_back(fb);
            faces2.push_back(b);
            faces2.push_back(a);
            faces2.push_back(fc);
            faces2.push_back(c);
            faces2.push_back(b);
            faces2.push_back(a);
            faces2.push_back(b);
            faces2.push_back(c);
        }
        indices = faces2;
    }

    for (auto& vertex : vertices) {
       auto newPosition = vertex.position() * this->radius;
       vertex.setPosition(newPosition);
    }

    return std::make_shared<Mesh>(vertices, indices);
}


SharedMesh GridGeometry::create() {
    std::vector<Vertex> vertices;

    float rowStepSize = width / resolutionX;
    float colStepSize = depth / resolutionZ;

    // Draw from centre
    float hdepth = depth / 2.0f;
    float hwidth = width / 2.0f;

    for (int col=0; col < resolutionZ + 1; col++) {
        float color[3] { 0.5f, 0.5f, 0.5f };

        if (col == resolutionZ / 2) {
            color[0] = 1.0;
            color[1] = 1.0;
            color[2] = 1.0;
        }

        float x = hdepth;
        float y = 0.0f;
        float z = colStepSize * col - hwidth;

        vertices.emplace_back(Vertex(-x, y, z, color[0], color[1], color[2]));
        vertices.emplace_back(Vertex(x, y, z, color[0], color[1], color[2]));
    }

    for (int row=0; row < resolutionX + 1; row++) {
        float color[3] { 0.5f, 0.5f, 0.5f };

        if (row == resolutionX / 2) {
            color[0] = 1.0;
            color[1] = 1.0;
            color[2] = 1.0;
        }

        float x = rowStepSize * row - hdepth;
        float y = 0.0f;
        float z = hwidth;

        vertices.emplace_back(Vertex(x, y, -z, color[0], color[1], color[2]));
        vertices.emplace_back(Vertex(x, y, z, color[0], color[1], color[2]));
    }

    return std::make_shared<Mesh>(vertices);
}


SharedMesh OriginGeometry::create() {
    std::vector<Vertex> vertices = {
        Vertex(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f),  // X-axis
        Vertex(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f),

        Vertex(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f),  // Y-axis
        Vertex(0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f),

        Vertex(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f),  // Z-axis
        Vertex(0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f),
    };

    for (auto& vertex : vertices) {
        auto newPosition = vertex.position() * this->size;
        vertex.setPosition(newPosition);
    }

    return std::make_shared<Mesh>(vertices);
}


/*      
           |
         __|__
    \  .   |   . /
      /    |    \
  ---|-----|-----|---
      \    |    /
     /  .__|__.  \
    /      |      \
           |

*/
SharedMesh PointLightGeometry::create() {
    std::vector<Vertex> vertices;

    unsigned int count = 10;
    QVector3D prevPos { 0, 0, 0};
    bool initialised { false };

    // Circle
    for (int ii=0; ii<count + 1; ii++) {
        float frac = static_cast<float>(ii) / count;

        float x = sin(frac * M_PI * 2);
        float y = cos(frac * M_PI * 2);
        float z = 0.0f;

        QVector3D pos { x, y, z};
        QVector3D col { 1.0f, 1.0f, 1.0f };

        if (!initialised) {
            initialised = true;
            prevPos = pos;
            continue;
        }

        vertices.emplace_back(Vertex(prevPos, col));
        vertices.emplace_back(Vertex(pos, col));
        prevPos = pos;

        // Sunshine
        vertices.emplace_back(Vertex(pos, col));
        vertices.emplace_back(Vertex(pos * 1.5f, col));
    }

    // Cross
    vertices.emplace_back(Vertex(-1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f));
    vertices.emplace_back(Vertex(1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f));
    vertices.emplace_back(Vertex(0.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f));
    vertices.emplace_back(Vertex(0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f));

    for (auto& vertex : vertices) {
        auto pos = vertex.position();
        pos *= this->size;
        vertex.setPosition(pos);
    }

    return std::make_shared<Mesh>(vertices);
}


SharedMesh DirectionalLightGeometry::create() {
    
    // Y
    //                   2
    // |  0             1|\
    // |  ---------------| > 3
    // |                5|/
    // |                 4
    // o ------------------------- X
    //
    std::vector<Vertex> arrow {
        Vertex(0.0f, 0.0f, -0.5f),  // 0
        Vertex(0.0f, 0.0f,  0.5f),  // 1

        Vertex(0.0f, 0.0f,  0.5f),  // 1
        Vertex(0.0f, 0.1f,  0.5f),  // 2

        Vertex(0.0f, 0.1f,  0.5f),  // 2
        Vertex(0.0f, 0.0f,  0.6f),  // 3

        Vertex(0.0f, 0.0f,  0.6f),  // 3
        Vertex(0.0f,-0.1f,  0.5f),  // 4

        Vertex(0.0f,-0.1f,  0.5f),  // 4
        Vertex(0.0f, 0.0f,  0.5f),  // 5
    };

    unsigned int count = 4;
    QVector3D color { 1.0f, 1.0f, 1.0f };
    std::vector<Vertex> vertices;
    for (int ii=0; ii<count; ii++) {
        for (auto& vertex : arrow) {
            float frac = static_cast<float>(ii) / count;

            // Make full circle
            float x = sin(frac * M_PI * 2) * this->size;
            float y = cos(frac * M_PI * 2) * this->size;
            float z = 0.0f;

            // Rotate randomly
            QVector3D pos = vertex.position() * this->size * 5.0f;
            QVector3D offset { x, y, z};
            auto rotation = QQuaternion::fromEulerAngles(0.0f, 180.0f, ii * 100.0f);
            pos = rotation.rotatedVector(pos);

            vertices.emplace_back(Vertex(pos + offset, color));
        }
    }

    return std::make_shared<Mesh>(vertices);
}


/* Camera Geometry
   ________
  /_______/ /\
  |       |/| |
  |_______|\| |
            \/

*/
SharedMesh CameraGeometry::create() {

    // TODO: Unfinished, not currently visible
    std::vector<Vertex> vertices {
        Vertex(-0.5f, -0.5f, -0.5f),  // Bottom
        Vertex( 0.5f, -0.5f, -0.5f),

        Vertex( 0.5f, -0.5f, -0.5f),
        Vertex( 0.5f, -0.5f, 0.5f),

        Vertex( 0.5f, -0.5f, 0.5f),
        Vertex(-0.5f, -0.5f, 0.5f),

        Vertex(-0.5f, -0.5f, 0.5f),
        Vertex(-0.5f, -0.5f, -0.5f),

        Vertex(-0.5f, 0.5f, -0.5f),   // Top
        Vertex( 0.5f, 0.5f, -0.5f),

        Vertex( 0.5f, 0.5f, -0.5f),
        Vertex( 0.5f, 0.5f, 0.5f),

        Vertex( 0.5f, 0.5f, 0.5f),
        Vertex(-0.5f, 0.5f, 0.5f),

        Vertex(-0.5f, 0.5f, 0.5f),
        Vertex(-0.5f, 0.5f, -0.5f),
    };

    return std::make_shared<Mesh>(vertices);
}


SharedMesh Manipulator1DGeometry::create() {
    std::vector<Vertex> vertices {
        Vertex(0.0f, 0.0f, 0.0f),
        Vertex(1.0f, 0.0f, 0.0f),

        Vertex(1.0f, 0.0f, 0.0f),
        Vertex(0.9f, 0.1f, 0.0f),

        Vertex(1.0f, 0.0f, 0.0f),
        Vertex(0.9f, -0.1f, 0.0f),
    };

    return std::make_shared<Mesh>(vertices);
}


SharedMesh NullGeometry::create() {
    std::vector<Vertex> vertices;
    return std::make_shared<Mesh>(vertices);
}