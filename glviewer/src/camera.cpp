#include "camera.h"
#include <algorithm>


void Camera::begin() {
    mStartRx = mRx;
    mStartRy = mRy;
    mStartOffset = mOffset;
    mStartDistance = mDistance;
}


void Camera::tumble(const float dx, const float dy) {
    mRx = -dy * mTumbleSensitivity + mStartRx;
    mRy = -dx * mTumbleSensitivity + mStartRy;
}


void Camera::track(const float dx, const float dy) {
    // TODO: This isn't working right
    // 1. The camera "pivot" isn't at the center of the view, but at the origin
    // 2. Tracking near an object is too fast; and too slow from far away
    mOffset[0] = -dx * mTrackingSensitivity;
    mOffset[1] = dy * mTrackingSensitivity;
    mOffset += mStartOffset;
}


void Camera::dolly(const float dx, const float dy) {
    // TODO: Dollying near an object is too fast; too slow from far away
    mDistance = std::max(0.5f, -dx * mDollySensitivity + mStartDistance);
}


void Camera::finish() {
}


QMatrix4x4 Camera::matrix() {
    QMatrix4x4 matrix;

    matrix.rotate(mRy, QVector3D(0.0f, 1.0f, 0.0f));
    matrix.rotate(mRx, QVector3D(1.0f, 0.0f, 0.0f));
    matrix.translate(mOffset);
    matrix.translate(QVector3D(0.0f, 0.0f, mDistance));

    return matrix;
}
