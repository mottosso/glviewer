#include "transform.h"


Transform::Transform(const QVector3D translation,
                     const QVector3D orientation,
                     const QVector3D scaling) : QMatrix4x4() {
    translate(translation);

    // XYZ rotate order
    rotate(orientation[0], QVector3D(1.0f, 0.0f, 0.0f));
    rotate(orientation[1], QVector3D(0.0f, 1.0f, 0.0f));
    rotate(orientation[2], QVector3D(0.0f, 0.0f, 1.0f));

    scale(scaling);
}


Transform::Transform(const QVector3D translation,
                     const QQuaternion orientation,
                     const QVector3D scaling) : QMatrix4x4() {
    translate(translation);
    rotate(orientation);
    scale(scaling);
}


const QVector3D Transform::translation() const {
    return QVector3D(
        Transform::operator()(0, 3),
        Transform::operator()(1, 3),
        Transform::operator()(2, 3)
    );
}


const QVector3D Transform::scaling() const {
    return QVector3D(
        Transform::operator()(0, 0),
        Transform::operator()(1, 1),
        Transform::operator()(2, 2)
    );
}


const QQuaternion Transform::rotation() const {
    auto mat = toGenericMatrix<3, 3>();
    return QQuaternion::fromRotationMatrix(mat);
}


void Transform::setTranslation(const QVector3D& translate) {
    setColumn(3, translate);
}


void Transform::setRotation(const QQuaternion& rotation) {
    this->setToIdentity();
    this->translate(translation());
    this->rotate(rotation);
    this->scale(scaling());
}


void Transform::setScale(const QQuaternion& scale) {

}
