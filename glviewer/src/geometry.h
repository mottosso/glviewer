#pragma once

#include <QVector>
#include <QString>
#include <QMatrix4x4>

#include <unordered_map>
#include <vector>
#include <memory>
#include <map>

#include "transform.h"
#include "vertex.h"

class Shader;
class Mesh;
class Node;
class Geometry;

using SharedShader = std::shared_ptr<Shader>;
using SharedMesh = std::shared_ptr<Mesh>;
using SharedNode = std::shared_ptr<Node>;
using SharedGeometry = std::shared_ptr<Geometry>;


class Shader {
public:
    Shader(const QString& name) : mName(name) {};

    enum Type {
        Vertex,
        Fragment,
        Geometry
    };

    // Accessor
    const QString& name() const { return mName; }
    std::shared_ptr<QByteArray> source(Type type) { return mShaders[type]; }

    void addStage(const Type type, const QString& fname) { stages[type] = fname; }
    // void addNode(const SharedNode node) { stages[type] = fname; }

    std::unordered_map<Type, QString> stages;
    // std::vector<SharedNode> nodes;

private:
    QString mName;
    Type mType;
    std::map<Type, std::shared_ptr<QByteArray>> mShaders;
};


enum class GeometryMode {
    Points, Lines, Triangles
};


enum class GeometryType {
    // Basic shapes
    Box, Sphere, Capsule, Plane, TriangleMesh,

    // Lights
    DirectionalLight, PointLight, SpotLight,

    // Debug
    Grid, Origin, Camera, Manipulator1D, Null
};


class Geometry {
public:
    virtual SharedMesh create() = 0;

    // Accessors
    const GeometryType type() const { return mType; }
    const GeometryMode mode() const { return mMode; }
    SharedMesh mesh() {
        if (!mMesh) mMesh = this->create();
        return mMesh;
    }

protected:
    Geometry(GeometryType type, GeometryMode mode = GeometryMode::Triangles)
        : mType(type), mMode(mode) {}
    GeometryType mType;
    GeometryMode mMode;
    SharedMesh mMesh;
};


class BoxGeometry : public Geometry {
public:
    BoxGeometry(const float width = 1.0f,
                const float height = 1.0f,
                const float depth = 1.0f)
        : Geometry(GeometryType::Box, GeometryMode::Triangles),
          dimensions(width, height, depth) {}

    SharedMesh create() override;
    QVector3D dimensions;
};


/*! Icosphere

From here: http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html

*/
class SphereGeometry : public Geometry {
public:
    SphereGeometry(const float radius = 1.0f, const int subdivisions = 2)
        : Geometry(GeometryType::Sphere, GeometryMode::Triangles),
          subdivisions(subdivisions),
          radius(radius) {}

    SharedMesh create() override;

    float radius;
    int subdivisions;

private:
    QVector3D projectPoint(const QVector3D& point) const;
    unsigned int getMiddlePoint(unsigned int a,
                                unsigned int b,
                                std::vector<Vertex>& vertices);
};


class CapsuleGeometry : public Geometry {};
class PlaneGeometry : public Geometry {};
class TriangleMeshGeometry : public Geometry {};


class GridGeometry : public Geometry {
public:
   GridGeometry(const float width = 1.0f,
                const float depth = 1.0f,
                const unsigned int resolutionX = 10,
                const unsigned int resolutionZ = 10)
       : Geometry(GeometryType::Grid, GeometryMode::Lines),
         width(width),
         depth(depth),
         resolutionX(resolutionX),
         resolutionZ(resolutionZ) {}

   SharedMesh create() override;

   float width;
   float depth;
   int resolutionX;
   int resolutionZ;
};


class OriginGeometry : public Geometry {
public:
    OriginGeometry(const float size = 0.1f)
        : Geometry(GeometryType::Origin, GeometryMode::Lines),
          size(size) {}

    SharedMesh create() override;
    float size;
};


class PointLightGeometry : public Geometry {
public:
    PointLightGeometry(const float size = 0.1f)
        : Geometry(GeometryType::PointLight, GeometryMode::Lines),
          size(size) {}

    SharedMesh create() override;
    float size;
};


class DirectionalLightGeometry : public Geometry {
public:
    DirectionalLightGeometry(const float size = 0.1f)
        : Geometry(GeometryType::DirectionalLight, GeometryMode::Lines),
          size(size) {}

    SharedMesh create() override;
    float size;
};


class CameraGeometry : public Geometry {
public:
    CameraGeometry(const float size = 0.1f)
        : Geometry(GeometryType::Camera, GeometryMode::Lines),
          size(size) {}

    SharedMesh create() override;
    float size;
};


class Manipulator1DGeometry : public Geometry {
public:
    Manipulator1DGeometry()
        : Geometry(GeometryType::Manipulator1D, GeometryMode::Lines) {}

    SharedMesh create() override;
};


class NullGeometry : public Geometry {
public:
    NullGeometry()
        : Geometry(GeometryType::Null, GeometryMode::Lines) {}

    SharedMesh create() override;
};


class Mesh {
public:
    Mesh(std::vector<Vertex> vertices,
         std::vector<unsigned int> indices) 
        : vertices(vertices),
          indices(indices) {}
    Mesh(std::vector<Vertex> vertices) : vertices(vertices) {}

    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
};
