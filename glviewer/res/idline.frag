// https://neure.dy.fi/wide_line.fs

#version 330

uniform vec2 lineWidth;
uniform vec3 uid;

in  vec2    v_start;
in  vec2    v_line;
in  vec4    v_color;
in  float   v_l2;
out vec4    out_color;

void main(void) {
    // float   t           = dot(gl_FragCoord.xy - v_start, v_line) / v_l2;
    // vec2    projection  = v_start + clamp(t, 0.0, 1.0) * v_line;
    // vec2    delta       = gl_FragCoord.xy - projection;
    // float   d2          = dot(delta, delta);
    // float   k           = clamp(lineWidth.y - d2, 0.0, 1.0);      //k = pow(k, 0.416666666);
    // float   endWeight   = step(abs(t * 2.0 - 1.0), 1);
    // float   alpha       = mix(k, 1.0, endWeight);

    out_color = vec4(uid, 1.0f);
    // out_color = vec4(v_color.rgb, alpha);
}
