#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightSpaceMatrix;

out vec3 vFragPos;
out vec2 vUv;
out vec3 vColor;
out vec4 vFragPosLightSpace;

void main() {
  gl_Position = projection * view * model * vec4(position, 1.0);
  vFragPos = (model * vec4(position, 1.0)).xyz;
  vColor = color;
  vUv = uv;

  // For shadows
  vFragPosLightSpace = lightSpaceMatrix * vec4(vFragPos, 1.0);
}
