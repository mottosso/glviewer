#version 330

in vec3 vFragPos;
in vec2 vUv;
in vec3 vColor;
in vec4 vFragPosLightSpace;

uniform vec4 objectColor;
uniform vec3 viewPos;
uniform sampler2D shadowMap;

struct Light {
    int type;
    vec3 position;
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform Light light;

out vec4 fColor;

vec3 computeLight(Light light, vec3 normal) {
    vec3 lightDir = light.type == 0 ? light.direction
                                    : normalize(light.position - vFragPos);

    vec3 viewDir = normalize(viewPos - vFragPos);
    vec3 reflectDir = reflect(-lightDir, normal); 


    // Multipliers
    float diff = max(dot(normal, lightDir), 0.0);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 128);

    vec3 diffuse = light.diffuse * diff;
    vec3 specular = light.specular * spec;

    return diffuse + specular;
}

float computeShadow(vec4 fragPosLightSpace, vec3 normal) {
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float currentDepth = projCoords.z;

    float bias = 0.000;
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 3; ++x) {
        for(int y = -1; y <= 3; ++y) {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
            shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;        
        }    
    }
    shadow /= 25.0;  // This many PCF samples

    if(projCoords.z > 1.0)
        shadow = 0.0;

    return shadow;
} 

void main() {
    float gamma = 2.2;
    vec3 normal = normalize(cross(dFdx(vFragPos), dFdy(vFragPos)));

    vec3 surface = computeLight(light, normal);
    float shadow = computeShadow(vFragPosLightSpace, normal);

    // Combine light and shadow
    surface = (1.0 - shadow) * surface * objectColor.xyz;

    // By including object color in ambience, it won't wash out the color
    surface += light.ambient * objectColor.xyz;

    // Apply gamma
    surface = pow(surface, vec3(1.0 / gamma));
    fColor = vec4(surface, 1.0);
}
