#version 330 core

in vec3 vColor;
out vec4 color;
uniform vec3 uid;


void main() {
    color = vec4(uid, 1.0);
}
