#version 330

layout(location = 0) in vec3 position;
layout(location = 2) in vec3 color;

uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;

out vec3 vColor;

void main() {
    vec3 offset = vec3(0.01, 0.01, 0.01);
    gl_Position = projection * view * model * vec4(position + offset, 1.0);
    vColor = color;
}
