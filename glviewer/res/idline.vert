// https://neure.dy.fi/wide_line.vs

#version 330

layout(location = 0) in vec3 position;
layout(location = 2) in vec3 color;

out vec4 vs_color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


void main() {
   	gl_Position = projection * view * model * vec4(position, 1.0);
    vs_color    = vec4(color, 1.0);
}
